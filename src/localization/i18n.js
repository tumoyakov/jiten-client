import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en';
import ru from './ru';

const resources = {
    en: en,
    ru: ru
};

i18n.use(initReactI18next).init({
    resources,
    lng: 'ru',
    keySeparator: false,
    interpolation: { escapeValue: false }
});

export default i18n;
