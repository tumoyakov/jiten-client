import useMediaQuery from "@material-ui/core/useMediaQuery";

import { downMediaQuery } from "../const";

export function useMobileView() {
  const matches = useMediaQuery(downMediaQuery);
  return matches;
}
