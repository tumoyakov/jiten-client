import { alphabet } from './alphabet';

export const engToFurigana = eng => {
    let value = eng;
    let result = '';
    const oneEl = ['a', 'i', 'u', 'e', 'o'];

    const findInAlphabet = value => {
        return alphabet.find(element => element.eng === value);
    };

    while (value.length > 0) {
        if (oneEl.includes(value[0])) {
            const alphabetItem = findInAlphabet(value[0]);
            result += alphabetItem ? alphabetItem.hir : value[0];
            value = value.slice(1);
        } else if (value[0] === 'n' && !oneEl.includes(value[1])) {
            const alphabetItem = findInAlphabet(value[0]);
            result += alphabetItem ? alphabetItem.hir : value[0];
            value = value.slice(1);
        } else if ((value[0] === 's' || value[0] === 'c') && value[1] === 'h') {
            const alphabetItem = findInAlphabet(value.slice(0, 3));
            result += alphabetItem ? alphabetItem.hir : value.slice(0, 3);
            value = value.slice(3);
        } else {
            const alphabetItem = findInAlphabet(value.slice(0, 2));
            result += alphabetItem ? alphabetItem.hir : value.slice(0, 2);
            value = value.slice(2);
        }
    }
    return result;
};

export const rusToFurigana = rus => {
    let value = rus;
    let result = '';
    const oneEl = ['а', 'и', 'у', 'э', 'о'];

    const findInAlphabet = value => {
        return alphabet.find(element => element.rus === value);
    };

    while (value.length > 0) {
        if (oneEl.includes(value[0])) {
            const alphabetItem = findInAlphabet(value[0]);
            result += alphabetItem ? alphabetItem.hir : value[0];
            value = value.slice(1);
        } else if (value[0] === 'н' && !oneEl.includes(value[1])) {
            const alphabetItem = findInAlphabet(value[0]);
            result += alphabetItem ? alphabetItem.hir : value[0];
            value = value.slice(1);
        } else {
            const alphabetItem = findInAlphabet(value.slice(0, 2));
            result += alphabetItem ? alphabetItem.hir : value.slice(0, 2);
            value = value.slice(2);
        }
    }
    return result;
};

export const hiraganaToKatakana = hir => {
    let value = hir;
    let result = '';

    const findInAlphabet = value => {
        return alphabet.find(element => element.hir === value);
    };

    while (value.length > 0) {
        const alphabetItem = findInAlphabet(value[0]);
        result += alphabetItem ? alphabetItem.kat : value[0];
        value = value.slice(1);
    }

    return result;
};

export const katakanaToHiragana = kat => {
    let value = kat;
    let result = '';

    const findInAlphabet = value => {
        return alphabet.find(element => element.kat === value);
    };

    while (value.length > 0) {
        const alphabetItem = findInAlphabet(value[0]);
        result += alphabetItem ? alphabetItem.hir : value[0];
        value = value.slice(1);
    }

    return result;
};
