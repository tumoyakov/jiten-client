export const alphabet = [
    { kat: 'ア', hir: 'あ', rus: 'а', eng: 'a' },
    { kat: 'イ', hir: 'い', rus: 'и', eng: 'i' },
    { kat: 'ウ', hir: 'う', rus: 'у', eng: 'u' },
    { kat: 'エ', hir: 'え', rus: 'э', eng: 'e' },
    { kat: 'オ', hir: 'お', rus: 'о', eng: 'o' },

    { kat: 'カ', hir: 'か', rus: 'ка', eng: 'ka' },
    { kat: 'キ', hir: 'き', rus: 'ки', eng: 'ki' },
    { kat: 'ク', hir: 'く', rus: 'ку', eng: 'ku' },
    { kat: 'ケ', hir: 'け', rus: 'кэ', eng: 'ke' },
    { kat: 'コ', hir: 'こ', rus: 'ко', eng: 'ko' },

    { kat: 'サ', hir: 'さ', rus: 'са', eng: 'sa' },
    { kat: 'シ', hir: 'し', rus: 'си', eng: 'shi' },
    { kat: 'ス', hir: 'す', rus: 'су', eng: 'su' },
    { kat: 'セ', hir: 'せ', rus: 'се', eng: 'se' },
    { kat: 'ソ', hir: 'そ', rus: 'со', eng: 'so' },

    { kat: 'タ', hir: 'た', rus: 'та', eng: 'ta' },
    { kat: 'チ', hir: 'ち', rus: 'чи', eng: 'chi' },
    { kat: 'ツ', hir: 'つ', rus: 'цу', eng: 'tu' },
    { kat: 'テ', hir: 'て', rus: 'те', eng: 'te' },
    { kat: 'ト', hir: 'と', rus: 'то', eng: 'to' },

    { kat: 'ナ', hir: 'な', rus: 'на', eng: 'na' },
    { kat: 'ニ', hir: 'に', rus: 'ни', eng: 'ni' },
    { kat: 'ヌ', hir: 'ぬ', rus: 'ну', eng: 'nu' },
    { kat: 'ネ', hir: 'ね', rus: 'нэ', eng: 'ne' },
    { kat: 'ノ', hir: 'の', rus: 'но', eng: 'no' },

    { kat: 'ハ', hir: 'は', rus: 'ха', eng: 'ha' },
    { kat: 'ヒ', hir: 'ひ', rus: 'хи', eng: 'hi' },
    { kat: 'フ', hir: 'ふ', rus: 'фу', eng: 'fu' },
    { kat: 'ヘ', hir: 'へ', rus: 'хэ', eng: 'he' },
    { kat: 'ホ', hir: 'ほ', rus: 'хо', eng: 'ho' },

    { kat: 'マ', hir: 'ま', rus: 'ма', eng: 'ma' },
    { kat: 'ミ', hir: 'み', rus: 'ми', eng: 'mi' },
    { kat: 'ム', hir: 'む', rus: 'му', eng: 'mu' },
    { kat: 'メ', hir: 'め', rus: 'мэ', eng: 'me' },
    { kat: 'モ', hir: 'も', rus: 'мо', eng: 'mo' },

    { kat: 'ヤ', hir: 'や', rus: 'я', eng: 'ya' },
    { kat: 'ユ', hir: 'ゆ', rus: 'ю', eng: 'yu' },
    { kat: 'ヨ', hir: 'よ', rus: 'ё', eng: 'yo' },

    { kat: 'ラ', hir: 'ら', rus: 'ра', eng: 'ra' },
    { kat: 'リ', hir: 'り', rus: 'ри', eng: 'ri' },
    { kat: 'ル', hir: 'る', rus: 'ру', eng: 'ru' },
    { kat: 'レ', hir: 'れ', rus: 'рэ', eng: 're' },
    { kat: 'ロ', hir: 'ろ', rus: 'ро', eng: 'ro' },

    { kat: 'ワ', hir: 'わ', rus: 'ва', eng: 'wa' },
    { kat: 'ヲ', hir: 'を', rus: 'о', eng: 'wo' },
    { kat: 'ン', hir: 'ん', rus: 'н', eng: 'n' }
];
