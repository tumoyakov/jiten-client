import { createMuiTheme } from "@material-ui/core";
import { downMediaQuery, upMediaQuery } from "../const";

export const AppTheme = createMuiTheme({
  palette: {
    primary: {
      light: "#C8E6C9",
      main: "#4CAF50",
      dark: "#388E3C"
    },
    secondary: {
      main: "#CDDC39"
    },
    divider: "#BDBDBD"
  },
  downMediaQuery,
  upMediaQuery,
  link: {
    color: "rgba(0, 0, 0, 0.87)",
    textDecoration: "none"
  },
  footerColor: {
    backgroundColor: "#edeeef"
  }
});
