import jitenServer from '../apis/jitenServer';

import {
  SIGN_IN,
  SIGN_UP,
  SIGN_OUT,
  REFRESH_USER,
  CHANGE_PASSWORD,
} from './types';

export const signin = (data) => async (dispatch) => {
  await jitenServer
    .post(`/signin`, { ...data })
    .then((response) => {
      dispatch({ type: SIGN_IN, payload: response.data });
    })
    .catch((err) => {
      switch (err.response.data.reason) {
        case 'InvalidPassword':
          return Promise.reject('invalidPassword');
        default:
          return Promise.reject('signInFailure');
      }
    });
};

export const signup = (data) => async (dispatch) => {
  try {
    const response = await jitenServer.post(`/signup`, { ...data });
    dispatch({ type: SIGN_UP, payload: response.data }); //TODO add checking failed response
  } catch (e) {
    return { error: e };
  }
};

export const signout = () => async (dispatch) => {
  dispatch({ type: SIGN_OUT });
};

export const refreshUser = () => async (dispatch) => {
  await jitenServer
    .get(`/user/me`)
    .then((response) => {
      dispatch({ type: REFRESH_USER, payload: response.data });
    })
    .catch((err) => {
      dispatch({ type: SIGN_OUT });
    });
};

export const changePassword = (data) => async (dispatch) => {
  try {
    const response = await jitenServer.post(`/user/password`, { ...data });
    dispatch({ type: CHANGE_PASSWORD, payload: response.data }); //TODO add checking failed response
  } catch (e) {
    return { error: e };
  }
};
