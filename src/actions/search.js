import jitenServer from '../apis/jitenServer';

import { SEARCH, CHANGE_PAGE } from './types';

export const search = (searchString) => async (dispatch) => {
  await jitenServer
    .post(`/search`, { search: searchString })
    .then((response) => {
      console.log(response);
      dispatch({
        type: SEARCH,
        payload: {
          searchString,
          results: response.data,
        },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const changePage = (page) => async (dispatch) => {
  dispatch({
    type: CHANGE_PAGE,
    payload: {
      page,
    },
  });
};
