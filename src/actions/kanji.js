import jitenServer from '../apis/jitenServer';

import { FETCH_KANJI, FETCH_KANJIS } from './types';

/**
 * Action creator for getting data for single kanji
 * @param {Number} id - id kanji in database
 */
export const fetchKanji = (id) => async (dispatch) => {
  const response = await jitenServer.get(`/kanji/item/${id}`);

  dispatch({ type: FETCH_KANJI, payload: response.data });
};

/**
 * Action creator for getting data for several kanji by array of their ids
 * @param {Array<Number>} ids - array of kanji id
 */
export const fetchKanjis = (ids) => async (dispatch) => {
  await jitenServer.post(`/kanjis/get`, { ids }).then((response) => {
    dispatch({ type: FETCH_KANJIS, payload: response.data });
  });
};
