import jitenServer from '../apis/jitenServer';

import { ADD_TEXT, FLUSH_READER, GET_WORDS_AND_KANJI_FROM_TEXT } from './types';

export const addText = (text) => (dispatch) => {
  dispatch({ type: ADD_TEXT, payload: { text } });
};

export const getKanjiAndWordsFromText = (text) => async (dispatch) => {
  await jitenServer
    .post(`/reader`, { text: text })
    .then((response) => {
      dispatch({ type: GET_WORDS_AND_KANJI_FROM_TEXT, payload: response.data });
    })
    .catch((err) => Promise.reject(err));
};

export const flushReader = () => (dispatch) => {
  dispatch({ type: FLUSH_READER });
};
