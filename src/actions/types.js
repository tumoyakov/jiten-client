//auth
export const SIGN_IN = 'SIGN_IN';
export const SIGN_UP = 'SIGN_UP';
export const SIGN_OUT = 'SIGN_OUT';
export const REFRESH_USER = 'REFRESH_USER';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';

//get items
export const FETCH_KANJI = 'FETCH_KANJI';
export const FETCH_KANJIS = 'FETCH_KANJIS';
export const FETCH_KANJIPARTS = 'FETCH_KANJIPARTS';

export const FETCH_WORD = 'FETCH_WORD';
export const FETCH_WORDS = 'FETCH_WORDS';

//search
export const SEARCH = 'SEARCH';
export const CHANGE_PAGE = 'CHANGE_PAGE';

//kanji lists
export const GET_KANJI_LISTS = 'GET_KANJI_LISTS';
export const ADD_KANJI_LIST = 'ADD_KANJI_LIST';
export const GET_KANJI_LIST = 'GET_KANJI_LIST';
export const UPDATE_KANJI_LIST = 'UPDATE_KANJI_LIST';
export const DELETE_KANJI_LIST = 'DELETE_KANJI_LIST';
export const DELETE_KANJI_FROM_LIST = 'DELETE_KANJI_FROM_LIST';
export const ADD_KANJI_TO_LIST = 'ADD_KANJI_TO_LIST';
export const CHANGE_PAGE_KANJI_LIST = 'CHANGE_PAGE_KANJI_LIST';
export const CHANGE_PAGE_IN_KANJI_LIST = 'CHANGE_PAGE_IN_KANJI_LIST'; //TODO refactor

//word lists
export const GET_WORD_LISTS = 'GET_WORD_LISTS';
export const ADD_WORD_LIST = 'ADD_WORD_LIST';
export const GET_WORD_LIST = 'GET_WORD_LIST';
export const UPDATE_WORD_LIST = 'UPDATE_WORD_LIST';
export const DELETE_WORD_LIST = 'DELETE_WORD_LIST';
export const DELETE_WORD_FROM_LIST = 'DELETE_WORD_FROM_LIST';
export const ADD_WORD_TO_LIST = 'ADD_WORD_TO_LIST';
export const CHANGE_PAGE_WORD_LIST = 'CHANGE_PAGE_WORD_LIST';
export const CHANGE_PAGE_IN_WORD_LIST = 'CHANGE_PAGE_IN_WORD_LIST';

//tests
export const SET_LIST_TO_TEST = 'SET_LIST_TO_TEST';
export const SEND_STATISTIC = 'SEND_STATISTIC';
export const GET_STAT = 'GET_STAT';
export const FLUSH_STAT = 'FLUSH_STAT';

//radicals
export const GET_RADICALS = 'GET_RADICALS';
export const FIND_KANJI_BY_RADICALS = 'FIND_KANJI_BY_RADICALS';
export const CLEAR_RESULT_SEARCH_BY_RADS = 'CLEAR_RESULT_SEARCH_BY_RADS';

//reader
export const ADD_TEXT = 'ADD_TEXT';
export const GET_WORDS_AND_KANJI_FROM_TEXT = 'GET_WORDS_AND_KANJI_FROM_TEXT';
export const FLUSH_READER = 'FLUSH_READER';
