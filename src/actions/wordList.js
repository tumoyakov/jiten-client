import jitenServer from '../apis/jitenServer';

import {
  GET_WORD_LISTS,
  ADD_WORD_LIST,
  GET_WORD_LIST,
  UPDATE_WORD_LIST,
  DELETE_WORD_LIST,
  DELETE_WORD_FROM_LIST,
  ADD_WORD_TO_LIST,
  CHANGE_PAGE_WORD_LIST,
  CHANGE_PAGE_IN_WORD_LIST,
} from '../actions/types';

export const getWordList = (id) => async (dispatch) => {
  const response = await jitenServer.get(`/word/list/${id}`);

  dispatch({ type: GET_WORD_LIST, payload: response.data });
};

export const addWordList = (name) => async (dispatch) => {
  const response = await jitenServer.post(`/word/list/create`, { name });

  dispatch({ type: ADD_WORD_LIST, payload: response.data });
};

export const updateWordList = (list) => async (dispatch) => {
  const response = await jitenServer.post(`/word/list/${list.id}`, {
    ...list,
  });

  dispatch({
    type: UPDATE_WORD_LIST,
    payload: { data: response.data },
  });
};

export const deleteWordList = (list) => async (dispatch) => {
  const response = await jitenServer.delete(`/word/list/${list.id}`, {
    ...list,
  });
  if (!response.error) {
    dispatch({ type: DELETE_WORD_LIST, payload: { id: list.id } });
  }
};

export const getWordLists = () => async (dispatch) => {
  const response = await jitenServer.get(`/word/lists`);
  console.log('response', response.data);
  dispatch({ type: GET_WORD_LISTS, payload: response.data });
};

export const addWordToList = (wordId, listId, listIndex) => async (
  dispatch
) => {
  await jitenServer
    .post(`/word/list/${listId}/${wordId}`)
    .then((response) => {
      console.log(response);
      dispatch({
        type: ADD_WORD_TO_LIST,
        payload: { data: response.data, listIndex },
      });
    })
    .catch((err) => {
      switch (err.response.data.error.name) {
        case 'This word is already in list':
          return Promise.reject('wordAlreadyInList');
        default:
          return Promise.reject(err.response.data.error.name);
      }
    });
};

export const deleteWordFromList = (wordId, listId) => async (dispatch) => {
  const response = await jitenServer.delete(
    `/delete/kanji/${listId}/${wordId}`
  );

  if (response.status === 200) {
    dispatch({
      type: DELETE_WORD_FROM_LIST,
      payload: { wordId, listId },
    });
  }
};

export const changePageWordList = (page) => async (dispatch) => {
  dispatch({
    type: CHANGE_PAGE_WORD_LIST,
    payload: {
      page,
    },
  });
};

export const changePageInWordList = (listId, page) => async (dispatch) => {
  dispatch({
    type: CHANGE_PAGE_IN_WORD_LIST,
    payload: {
      listId,
      page,
    },
  });
};
