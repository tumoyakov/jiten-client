import jitenServer from '../apis/jitenServer';

import {
  GET_RADICALS,
  FIND_KANJI_BY_RADICALS,
  CLEAR_RESULT_SEARCH_BY_RADS,
} from './types';

export const getRadicals = (id) => async (dispatch) => {
  const response = await jitenServer.get(`/kanji/radk`);

  dispatch({ type: GET_RADICALS, payload: response.data.rads });
};

export const findKanjiByRads = (rads) => async (dispatch) => {
  if (rads.length !== 0) {
    const response = await jitenServer.post(`/kanji/find-by-rads`, { rads });

    dispatch({ type: FIND_KANJI_BY_RADICALS, payload: response.data.kanjis });
  } else {
    dispatch({ type: CLEAR_RESULT_SEARCH_BY_RADS });
  }
};

export const clearResultSearchByRads = () => async (dispatch) => {
  dispatch({ type: CLEAR_RESULT_SEARCH_BY_RADS });
};
