import { SET_LIST_TO_TEST, FLUSH_STAT, GET_STAT } from '../actions/types';
import jitenServer from '../apis/jitenServer';

export const setListToTest = (list, type) => async (dispatch) => {
  dispatch({
    type: SET_LIST_TO_TEST,
    payload: {
      list,
      type,
    },
  });
};

export const sendStatistic = (statistic) => async (dispatch, getState) => {
  await jitenServer.post('/statistic', statistic);
};

export const getStatistic = (type, itemId) => async (dispatch, getState) => {
  await jitenServer
    .get(`/statistic/${type}/${itemId}`)
    .then((response) => {
      dispatch({ type: GET_STAT, payload: response.data });
    })
    .catch((err) => {
      switch (err.response.data.reason) {
        default:
          return Promise.reject('getStatFail');
      }
    });
};

export const flushStatistic = () => (dispatch) => {
  dispatch({ type: FLUSH_STAT });
};
