import jitenServer from '../apis/jitenServer';

import { FETCH_WORD, FETCH_WORDS } from './types';

/**
 * Action creator for getting data for single word
 * @param {Number} id - id kanji in database
 */
export const fetchWord = (id) => async (dispatch) => {
  const response = await jitenServer.get(`/word/item/${id}`);

  dispatch({ type: FETCH_WORD, payload: response.data });
};

/**
 * Action creator for getting data for several words by array of their ids
 * @param {Array<Number>} ids - array of kanji id
 */
export const fetchWords = (ids) => async (dispatch) => {
  await jitenServer.post(`/words/get`, { ids }).then((response) => {
    dispatch({ type: FETCH_WORDS, payload: response.data });
  });
};
