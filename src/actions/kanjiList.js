import jitenServer from '../apis/jitenServer';

import {
  GET_KANJI_LISTS,
  ADD_KANJI_LIST,
  GET_KANJI_LIST,
  UPDATE_KANJI_LIST,
  DELETE_KANJI_LIST,
  ADD_KANJI_TO_LIST,
  DELETE_KANJI_FROM_LIST,
  CHANGE_PAGE_KANJI_LIST,
  CHANGE_PAGE_IN_KANJI_LIST,
} from '../actions/types';

export const getKanjiList = (id) => async (dispatch) => {
  const response = await jitenServer.get(`/kanji/list/${id}`);

  dispatch({ type: GET_KANJI_LIST, payload: response.data });
};

export const addKanjiList = (name) => async (dispatch) => {
  const response = await jitenServer.post(`/kanji/list/create`, { name });

  dispatch({ type: ADD_KANJI_LIST, payload: response.data });
};

export const updateKanjiList = (list) => async (dispatch) => {
  const response = await jitenServer.post(`/kanji/list/${list.id}`, {
    ...list,
  });

  dispatch({
    type: UPDATE_KANJI_LIST,
    payload: { data: response.data },
  });
};

export const deleteKanjiList = (list) => async (dispatch) => {
  const response = await jitenServer.delete(`/kanji/list/${list.id}`, {
    ...list,
  });
  if (!response.error) {
    dispatch({ type: DELETE_KANJI_LIST, payload: { id: list.id } });
  }
};

export const getKanjiLists = () => async (dispatch) => {
  console.log('token', localStorage.getItem('token'));
  const response = await jitenServer.get(`/kanji/lists`);

  dispatch({ type: GET_KANJI_LISTS, payload: response.data });
};

export const addKanjiToList = (kanjiId, listId, listIndex) => async (
  dispatch
) => {
  const response = await jitenServer
    .post(`/add/kanji`, {
      id: listId,
      kanjiId,
    })
    .then((response) => {
      dispatch({
        type: ADD_KANJI_TO_LIST,
        payload: { data: response.data, listIndex },
      });
    })
    .catch((err) => {
      switch (err.response.data.error.name) {
        case 'This kanji is already in list':
          return Promise.reject('kanjiAlreadyInList');
        default:
          return Promise.reject(err.response.data.error.name);
      }
    });
};

export const deleteKanjiFromList = (kanjiId, listId) => async (dispatch) => {
  const response = await jitenServer.delete(
    `/delete/kanji/${listId}/${kanjiId}`
  );

  if (response.status === 200) {
    dispatch({
      type: DELETE_KANJI_FROM_LIST,
      payload: { kanjiId, listId },
    });
  }
};

export const changePageKanjiList = (page) => async (dispatch) => {
  dispatch({
    type: CHANGE_PAGE_KANJI_LIST,
    payload: {
      page,
    },
  });
};

export const changePageInKanjiList = (listId, page) => async (dispatch) => {
  dispatch({
    type: CHANGE_PAGE_IN_KANJI_LIST,
    payload: {
      listId,
      page,
    },
  });
};
