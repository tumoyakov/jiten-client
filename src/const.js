export const downSwitch = "sm";
export const upSwitch = "md";

export const upMediaQuery = "@media (min-width:960px)";
export const downMediaQuery = "@media (max-width:959.95px)";

export const downProps = { [downSwitch + "Down"]: true };
export const upProps = { [upSwitch + "Up"]: true };

export const noop = () => {};
