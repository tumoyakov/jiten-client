import axios from 'axios';

export default axios.create({
  baseURL: 'https://195.133.196.70:443',
  headers: {
    'Access-Control-Allow-Origin': '*',
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  },
});
