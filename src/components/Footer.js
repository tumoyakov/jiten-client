import React, { useState } from 'react';
import { Container, Typography, Button, Link, makeStyles } from '@material-ui/core';
import { Link as RouterLink  } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import LangMenu from './common/menu/LangMenu';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        height: '56px',
        flexGrow: 1,
        ...theme.footerColor
    },
    footerContainer: {
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    footerElement: {
        marginRight: theme.spacing(2)
    }
}));

const Footer = props => {
    const classes = useStyles();

    const { t } = useTranslation();

    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <Container className={classes.footerContainer} maxWidth="md">
                <Typography variant="body2" className={classes.footerElement}>
                    © Jiten 2020
                </Typography>
                <Button className={classes.footerElement} size="small" variant="outlined" onClick={handleClick}>
                    Язык/Language
                </Button>
                <LangMenu anchorEl={anchorEl} handleClose={handleClose} />
                <Link component={RouterLink} to="/about">О проекте</Link>
            </Container>
        </div>
    );
};

export default Footer;
