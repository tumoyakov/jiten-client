import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';
const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        minWidth: 275
    }
}));

const Screen404 = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Typography variant="h3" component="h2">
                Страница не найдена. Ошибка 404.
            </Typography>
        </div>
    );
};

export default Screen404;
