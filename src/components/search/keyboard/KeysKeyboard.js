import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Typography, makeStyles } from '@material-ui/core';

import Loading from '../../common/Loading';
import Radical from './Radical';
import RadicalStrokeCount from './RadicalStrokeCount';
import RadicalRefresh from './RadicalRefresh';
import ResultItem from './ResultItem';
import {
  getRadicals,
  findKanjiByRads,
  clearResultSearchByRads,
} from '../../../actions';

const useStyle = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
  },
  keyboard: {
    marginTop: theme.spacing(1),
    width: '100%',
    backgroundColor: '#ddd',
    borderRadius: '5px',
    display: 'flex',
    flexDirection: 'row',
  },
  keyboardInner: {
    padding: '4px',
    display: 'flex',
    width: 'fit-content',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  result: {
    marginTop: theme.spacing(1),
    width: '100%',
    maxHeight: '170px',
    backgroundColor: '#ddd',
    borderRadius: '5px',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    overflowY: 'auto',
  },
}));

const KeysKeyboard = (props) => {
  const {
    radicals,
    kanjis,
    getRadicals,
    findKanjiByRads,
    clearResultSearchByRads,
    onAddKanji,
  } = props;
  const classes = useStyle();

  const [checkedKeys, setKeyChecked] = useState([]);
  const [searched, setSearched] = useState(false);

  useEffect(() => {
    if (!radicals || radicals.length === 0) {
      getRadicals();
    }
  }, [radicals]);

  if (!radicals || radicals.length === 0) {
    return <Loading />;
  }

  function handleClick(radId) {
    if (checkedKeys.includes(radId)) {
      const newArr = checkedKeys.filter((key) => key !== radId);
      setKeyChecked(newArr);
      findKanjiByRads(newArr);
    } else {
      const newArr = [...checkedKeys, radId];
      setKeyChecked(newArr);
      findKanjiByRads(newArr);
    }
  }

  function handleRefresh() {
    setKeyChecked([]);
    clearResultSearchByRads();
  }

  function renderRadicals() {
    let result = [];
    result.push(
      <RadicalRefresh key={'refresh-keyboard'} onClick={handleRefresh} />
    );
    for (let i = 1; i < 14; i++) {
      result.push(<RadicalStrokeCount key={`count-${i}`} count={i} />);
      radicals
        .filter((radical) => radical.strokeCount === i)
        .forEach((rad) => {
          const checked = checkedKeys.includes(rad.id);
          result.push(
            <Radical
              key={rad.id}
              radical={rad}
              checked={checked}
              onClick={handleClick}
            />
          );
        });
    }
    return result;
  }

  function renderResults() {
    if (!kanjis || kanjis.length === 0) {
      if (searched) {
        return (
          <div>
            <Typography variant="h6">{`not found`}</Typography>
          </div>
        );
      } else {
        return null;
      }
    }

    let result = [];
    let maxStrokeCount = kanjis.reduce((acc, kanji) => {
      return acc < kanji.strokeCount ? kanji.strokeCount : acc;
    }, 1);

    for (let i = 1; i < maxStrokeCount; i++) {
      const filteredKanjis = kanjis.filter((kanji) => kanji.strokeCount === i);
      if (filteredKanjis.length > 0)
        result.push(<RadicalStrokeCount key={`count-${i}`} count={i} />);
      filteredKanjis.forEach((kanji) => {
        result.push(
          <ResultItem
            key={kanji.id}
            literal={kanji.literal}
            onClick={onAddKanji}
          />
        );
      });
    }

    console.log(result, kanjis);

    return <div className={classes.result}>{result}</div>;
  }

  return (
    <div className={classes.root}>
      {renderResults()}
      <div className={classes.keyboard}>
        <div className={classes.keyboardInner}>{renderRadicals()}</div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    radicals: state.radKeys.radicals,
    kanjis: state.radKeys.kanjis,
  };
};

export default connect(mapStateToProps, {
  getRadicals,
  findKanjiByRads,
  clearResultSearchByRads,
})(KeysKeyboard);
