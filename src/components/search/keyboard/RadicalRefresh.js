import React from 'react';
import { Typography, makeStyles, Icon } from '@material-ui/core';
import { Refresh } from '@material-ui/icons';

const useStyle = makeStyles((theme) => ({
  root: {
    width: '30px',
    height: '30px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '2px',
    background: '#000',
    color: '#fff',
    borderRadius: '5px',
    cursor: 'pointer',
  },
}));

const RadicalRefresh = (props) => {
  const { onClick } = props;
  const classes = useStyle();
  return (
    <div className={classes.root} onClick={onClick}>
      <Refresh />
    </div>
  );
};

export default RadicalRefresh;
