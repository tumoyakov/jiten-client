import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
  root: {
    width: '30px',
    height: '30px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '2px',
    background: '#000',
    color: '#fff',
    borderRadius: '5px',
  },
}));

const RadicalStrokeCount = (props) => {
  const { count } = props;
  const classes = useStyle();
  return (
    <div className={classes.root}>
      <Typography variant="subtitle1">{count}</Typography>
    </div>
  );
};

export default RadicalStrokeCount;
