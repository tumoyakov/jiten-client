import React from 'react';
import clsx from 'clsx';
import { Typography, makeStyles } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
  root: {
    boxSizing: 'border-box',
    width: '30px',
    height: '30px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '2px',
    background: '#fff',
    borderRadius: '5px',
    cursor: 'pointer',
  },
  checked: {
    border: '2px solid #4CAF50',
  },
}));

const ResultItem = (props) => {
  const { literal, onClick } = props;
  const classes = useStyle();
  return (
    <div className={classes.root} onClick={() => onClick(literal)}>
      <Typography variant="h6">{literal}</Typography>
    </div>
  );
};

export default ResultItem;
