import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import {
  Card,
  CardContent,
  FormControl,
  OutlinedInput,
  InputAdornment,
  ButtonBase,
  Typography,
  makeStyles,
  IconButton,
} from '@material-ui/core';
import { Search, ArrowDropDown, ArrowDropUp } from '@material-ui/icons';

import { clearResultSearchByRads } from '../../actions';
import KeysKeyboard from './keyboard/KeysKeyboard';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    minWidth: 275,
  },
  content: {
    padding: theme.spacing(1),
    '&:last-child': {
      paddingBottom: theme.spacing(1),
    },
  },
  keysButtonBase: {
    marginTop: theme.spacing(1),
  },
  keysButton: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  searchInput: {
    fontSize: '1.2em',
  },
}));

const SearchField = (props) => {
  const classes = useStyles();
  const [isKeyboardShown, setKeyboardShown] = useState(false);
  const [search, setSearch] = useState('');
  const inputRef = useRef();

  const { value, clearResultSearchByRads } = props;

  useEffect(() => {
    console.log(value);
    if (!search && value) setSearch(value);
  }, []);

  const handleKeyButton = () => {
    if (isKeyboardShown) clearResultSearchByRads();
    setKeyboardShown(!isKeyboardShown);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.handleSearch(search);
  };

  const renderKeyboard = () => {
    return isKeyboardShown ? (
      <KeysKeyboard onAddKanji={handleAddingKanjiFromKeyboard} />
    ) : null;
  };

  const handleAddingKanjiFromKeyboard = (kanji) => {
    const selectionStart = inputRef.current.selectionStart;
    console.log(selectionStart);
    console.log(search);
    const firstPart = search.slice(0, selectionStart);
    const lastPart = search.slice(selectionStart, search.length);

    setSearch(`${firstPart}${kanji}${lastPart}`);
  };

  const renderSearchField = () => {
    return (
      <FormControl fullWidth variant="outlined">
        <OutlinedInput
          id="search-input"
          inputRef={inputRef}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          autoFocus={true}
          className={classes.searchInput}
          startAdornment={
            <IconButton position="start" onClick={handleSubmit}>
              <Search />
            </IconButton>
          }
        />
      </FormControl>
    );
  };

  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <form onSubmit={handleSubmit}>{renderSearchField()}</form>
        <ButtonBase
          className={classes.keysButtonBase}
          onClick={handleKeyButton}>
          <div className={classes.keysButton}>
            <Typography variant="body2" component="span">
              Ввод по ключам
            </Typography>
            {!isKeyboardShown ? <ArrowDropDown /> : <ArrowDropUp />}
          </div>
        </ButtonBase>
        {renderKeyboard()}
      </CardContent>
    </Card>
  );
};

export default connect(null, { clearResultSearchByRads })(SearchField);
