import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Card, Button, makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';

import WordData from './WordData';
import AddToListDialog from '../../common/AddToListDialog';
import { kElementsToString, rElementsToString } from '../../words/WordUtils';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  menuButton: {
    textDecoration: 'none',
    marginRight: theme.spacing(1),
  },
}));

const WordCard = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  const { data, session } = props;

  const [openDialog, setDialogOpen] = useState(false);

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  if (!data) return null;

  const keb = kElementsToString(data.kanjiElements);
  const reb = rElementsToString(data.readElements);

  const locale = t('card', { returnObjects: true });

  const renderBottomCardMenu = () => {
    return (
      <div className={classes.menu}>
        {session.auth ? (
          <React.Fragment>
            <Button
              variant="outlined"
              size="small"
              color="primary"
              className={classes.menuButton}
              onClick={() => setDialogOpen(true)}>
              {locale.toList}
            </Button>
            <AddToListDialog
              itemId={data.id}
              type="word"
              handleClose={handleCloseDialog}
              open={openDialog}
            />
          </React.Fragment>
        ) : null}
        <Link to={`/word/${data.id}`} className={classes.menuButton}>
          <Button variant="outlined" size="small" color="primary">
            {locale.detail}
          </Button>
        </Link>
      </div>
    );
  };

  return (
    <Card className={classes.root}>
      <div className={classes.cardContent}>
        <WordData keb={keb} reb={reb} senses={data.senses} locale={locale} />
        {renderBottomCardMenu()}
      </div>
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    session: state.session,
  };
};

export default connect(mapStateToProps, {})(WordCard);
