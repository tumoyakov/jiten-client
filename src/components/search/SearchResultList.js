import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import {
  Typography,
  Tab,
  Tabs,
  CircularProgress,
  makeStyles,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';

import SearchKanjiCard from './SearchKanjiCard';
import SearchWordCard from './word/SearchWordCard';
import { changePage } from '../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  firstView: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pagination: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(1),
    width: '100%',
  },
}));

function calculateCount(tabValue, kanjis, words) {
  return kanjis && words ? (tabValue === 0 ? words.length : kanjis.length) : 0;
}

function isEmptyArray(array) {
  return !array || array.length === 0;
}

const SearchResultList = (props) => {
  const classes = useStyles();

  const { t } = useTranslation();
  const locale = t('SearchResultList', { returnObjects: true });

  const {
    result: { kanjis, words },
    page,
    changePage,
  } = props;

  const [tabValue, setTabValue] = useState(0);
  const [count, setCount] = useState(calculateCount(tabValue, kanjis, words));

  const handleChangeTabValue = (event, newValue) => {
    setTabValue(newValue);
    setCount(calculateCount(newValue, kanjis, words));
    changePage(1);
  };

  const renderWords = (list) => {
    return list && list.length > 0
      ? list
          .slice((page - 1) * 10, page * 10)
          .map((item) => <SearchWordCard data={item} key={`word ${item.id}`} />)
      : null;
  };

  const renderKanjis = (list) => {
    return list && list.length > 0
      ? list
          .slice((page - 1) * 10, page * 10)
          .map((item) => (
            <SearchKanjiCard data={item} key={`kanji ${item.id}`} />
          ))
      : null;
  };

  const renderEmptyViewMessage = () => {
    return (
      <div className={clsx(classes.root, classes.firstView)}>
        <Typography variant="body2" component="div">
          По данному запросу ничего не найдено.
        </Typography>
      </div>
    );
  };

  const handlePageChange = (event, page) => {
    changePage(page);
  };

  function renderKanjisResult() {
    if (isEmptyArray(kanjis)) return renderEmptyViewMessage();
    return (
      <React.Fragment>
        {renderKanjis(kanjis)}
        <div className={classes.pagination}>
          {count.kanjis > 0 ? (
            <Pagination
              page={page}
              count={count}
              onChange={handlePageChange}
              showFirstButton
              showLastButton
            />
          ) : null}
        </div>
      </React.Fragment>
    );
  }

  function renderWordsResult() {
    if (isEmptyArray(words)) return renderEmptyViewMessage();

    return (
      <React.Fragment>
        <div>{renderWords(words)}</div>
        <div className={classes.pagination}>
          {count.words > 0 ? (
            <Pagination
              page={page}
              count={count}
              onChange={handlePageChange}
              showFirstButton
              showLastButton
            />
          ) : null}
        </div>
      </React.Fragment>
    );
  }

  return (
    <div className={classes.root}>
      <Tabs
        value={tabValue}
        onChange={handleChangeTabValue}
        indicatorColor="primary"
        textColor="primary">
        <Tab label={locale.word} />
        <Tab label={locale.kanji} />
      </Tabs>
      {tabValue === 0 ? renderWordsResult() : renderKanjisResult()}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    page: state.search.page,
  };
};

export default connect(mapStateToProps, { changePage })(SearchResultList);
