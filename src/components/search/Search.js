import React, { useState, useEffect, Fragment } from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import { Typography, CircularProgress, makeStyles } from '@material-ui/core';

import SearchField from './SearchField';
import SearchResultList from './SearchResultList';
import SearchResultSelector from './SearchResultSelector';
import { search, fetchKanjis, fetchWords } from '../../actions';

const useStyle = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
  },
  firstView: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loading: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    marginRight: theme.spacing(1),
  },
}));

const Search = (props) => {
  const classes = useStyle();
  const { results, search, searchString } = props;

  const [loading, setLoading] = useState(false);

  const [firstView, setFirstView] = useState(true);
  const [selectedResultIndex, setSelectedResultIndex] = useState(0);

  useEffect(() => {
    if (firstView && results.length !== 0) {
      setFirstView(false);
    }
  }, [results, firstView]);

  function selectResult(index) {
    setSelectedResultIndex(index);
  }

  const handleSearch = (searchStr) => {
    if (!searchStr || searchStr === '') return;
    setFirstView(false);

    setLoading(true);
    search(searchStr)
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  function renderFirstViewMessage() {
    return (
      <div className={clsx(classes.root, classes.firstView)}>
        <Typography variant="body2" component="div">
          Введите в строке поиска любое слово на русском или японском, либо его
          транскрипции на английском или русском языках.
        </Typography>
      </div>
    );
  }

  function renderEmptyViewMessage() {
    return (
      <div className={clsx(classes.root, classes.firstView)}>
        <Typography variant="body2" component="div">
          По данному запросу ничего не найдено.
        </Typography>
      </div>
    );
  }

  const renderLoadingMessage = () => {
    return (
      <div className={clsx(classes.root, classes.loading)}>
        <CircularProgress className={classes.progress} />
        <Typography variant="subtitle1">Загрузка</Typography>
      </div>
    );
  };

  function renderResults() {
    if (loading) return renderLoadingMessage();

    console.log('selected result', selectedResultIndex, results);

    return results && results.length > 0 ? (
      <Fragment>
        <SearchResultSelector
          results={results}
          selectedResultIndex={selectedResultIndex}
          selectResult={selectResult}
        />
        <SearchResultList
          result={results[selectedResultIndex]}
          firstView={firstView}
        />
      </Fragment>
    ) : firstView ? (
      renderFirstViewMessage()
    ) : (
      renderEmptyViewMessage()
    );
  }

  return (
    <div className={classes.root}>
      <SearchField handleSearch={handleSearch} value={searchString} />
      {renderResults()}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    searchString: state.search.searchString,
    results: state.search.results,
  };
};

export default connect(mapStateToProps, { search })(Search);
