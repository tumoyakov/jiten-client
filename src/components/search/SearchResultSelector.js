import React from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core';

import { changePage } from '../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  word: {
    boxSizing: 'border-box',
    marginRight: theme.spacing(1),
    fontSize: '1.2em',
    cursor: 'pointer',
  },
  selected: {
    color: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: '5px',
    padding: theme.spacing(1),
  },
}));

const SearchResultSelector = (props) => {
  const classes = useStyles();

  const { results, selectedResultIndex, selectResult, changePage } = props;

  if (!results) return null;

  function selectWord(resultIndex) {
    if (selectedResultIndex !== resultIndex) {
      selectResult(resultIndex);
      changePage(1);
    }
  }

  function renderWord(word, index) {
    return (
      <span
        key={`word-selector-${index}`}
        className={
          selectedResultIndex === index
            ? clsx(classes.selected, classes.word)
            : classes.word
        }
        onClick={() => selectWord(index)}>
        {word}
      </span>
    );
  }

  function renderResults() {
    return results.map((result, index) => {
      return (
        result.word &&
        result.word.surface_form &&
        renderWord(result.word.surface_form, index)
      );
    });
  }

  return <div className={classes.root}>{renderResults()}</div>;
};

export default connect(null, { changePage })(SearchResultSelector);
