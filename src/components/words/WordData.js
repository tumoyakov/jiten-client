import React, { Fragment } from 'react';
import { Typography, Divider, makeStyles } from '@material-ui/core';
import { kElementsToString, rElementsToString } from './WordUtils';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  item: {
    width: '100%',
    marginBottom: theme.spacing(1),
  },
  itemText: {
    fontWeight: '400',
  },
  itemHeader: {
    marginBottom: theme.spacing(1),
  },
}));

const WordData = (props) => {
  const classes = useStyles();

  const { kanjiElements, readElements, senses } = props.word;

  const { t } = useTranslation();

  const locale = t('WordDetailData', { returnObjects: true });

  const keb = kElementsToString(kanjiElements);
  const reb = rElementsToString(readElements);

  const renderMain = () => {
    if (kanjiElements[0] && kanjiElements[0].keb)
      return (
        <div className={classes.item}>
          <Typography variant="h3" className={classes.itemText}>
            {kanjiElements[0].keb}
          </Typography>
        </div>
      );

    return (
      <div className={classes.item}>
        <Typography variant="h3" className={classes.itemText}>
          {readElements[0].reb}
        </Typography>
      </div>
    );
  };

  const renderKanjiElements = () => {
    if (!kanjiElements || kanjiElements.length === 0) return null;

    return (
      <div className={classes.item}>
        <Typography variant="subtitle1" className={classes.itemHeader}>
          {locale.kElements}
          <Divider />
        </Typography>
        {kanjiElements.map((element) => {
          return (
            <Typography variant="subtitle1" className={classes.itemText}>
              {element.keb}
            </Typography>
          );
        })}
      </div>
    );
  };

  const renderReadings = () => {
    if (!readElements || readElements.length === 0) return null;
    return (
      <div className={classes.item}>
        <Typography variant="subtitle1" className={classes.itemHeader}>
          {locale.rElements}
          <Divider />
        </Typography>
        {readElements.map((element) => {
          return (
            <Typography variant="subtitle1" className={classes.itemText}>
              {element.reb}
            </Typography>
          );
        })}
      </div>
    );
  };

  const renderSenses = () => {
    if (!senses || senses.length === 0) return null;
    let allGlosses = [];
    senses.forEach((sense) => {
      sense.glosses &&
        sense.glosses
          .filter((gloss) => gloss.lang === 'eng' || gloss.lang === 'rus')
          .forEach((gloss) => allGlosses.push(gloss));
    });

    const engGlosses = allGlosses.filter((gloss) => gloss.lang === 'eng');
    const rusGlosses = allGlosses.filter((gloss) => gloss.lang === 'rus');

    return (
      <div className={classes.item}>
        <Typography variant="subtitle1" className={classes.itemHeader}>
          {locale.glosses}
          <Divider />
        </Typography>
        {engGlosses && engGlosses.length > 0 ? (
          <Fragment>
            <Typography variant="subtitle1" className={classes.itemText}>
              eng:
            </Typography>
            {engGlosses.map((gloss, index) => renderGloss(gloss, index))}
          </Fragment>
        ) : null}
        {rusGlosses && rusGlosses.length > 0 ? (
          <Fragment>
            <Typography variant="subtitle1" className={classes.itemText}>
              rus:
            </Typography>
            <Divider />
            {rusGlosses.map((gloss, index) => renderGloss(gloss, index))}
          </Fragment>
        ) : null}
      </div>
    );
  };

  const renderGloss = (gloss, index) => (
    <Typography variant="subtitle2" className={classes.itemText}>
      {`${index + 1}) ${gloss.data} ( ${gloss.source} )`}
    </Typography>
  );

  return (
    <div className={classes.root}>
      {renderMain()}
      {renderKanjiElements()}
      {renderReadings()}
      {renderSenses()}
    </div>
  );
};

export default WordData;
