import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Card, Typography, makeStyles } from '@material-ui/core';
import { fetchWord } from '../../actions';
import WordData from './WordData';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    boxSizing: 'border-box',
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
  },
  main: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2),
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: theme.spacing(2),
  },
  word: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  actions: {
    marginLeft: 'auto',
  },
  title: {
    fontWeight: '600',
  },
  link: {
    marginLeft: 'auto',
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
  },
}));

const WordDetail = (props) => {
  const { id } = props.match.params;
  const classes = useStyles();

  useEffect(() => {
    props.fetchWord(id);
  }, []);

  if (!props.word) {
    return <div>Loading...</div>; //todo create loading component
  }

  return (
    <div>
      <Card className={classes.root}>
        <div className={classes.main}>
          <WordData word={props.word} />
        </div>
      </Card>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    word: state.words[ownProps.match.params.id],
  };
};

export default connect(mapStateToProps, { fetchWord })(WordDetail);
