/**
 * Function that creates string from array of kanji elements of word
 * @param {Array<KanjiElement>} kElements - array of kanji elements
 * @return {String} result string
 */
export function kElementsToString(kElements) {
  return (
    kElements &&
    kElements.reduce((acc, item, index) => {
      return index !== kElements.length - 1
        ? `${acc} ${item.keb};`
        : `${acc} ${item.keb}`;
    }, '')
  );
}

/**
 * Function that creates string from array of reading elements of word
 * @param {Array<ReadElement>} rElements - array of reading elements
 * @return {String} result string
 */
export function rElementsToString(rElements) {
  return (
    rElements &&
    rElements.reduce((acc, item, index) => {
      return index !== rElements.length - 1
        ? `${acc} ${item.reb};`
        : `${acc} ${item.reb}`;
    }, '')
  );
}
