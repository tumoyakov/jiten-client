import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core';

import ReadingToolbar from './ReadingToolbar';
import TextReader from './TextReader';
import { getKanjiAndWordsFromText, addText, flushReader } from '../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    boxSizing: 'border-box',
    height: '100%',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const ReadingIndex = (props) => {
  const classes = useStyles();

  const { text, data, getKanjiAndWordsFromText, addText, flushReader } = props;

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (text !== null && data.length === 0) {
      setLoading(true);
      getKanjiAndWordsFromText(text)
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    }
  }, [text, data]);

  return (
    <div className={classes.root}>
      <ReadingToolbar addText={addText} />
      <TextReader text={text} data={data} />
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    text: state.reader.text,
    data: state.reader.data,
  };
};

export default connect(mapStateToProps, {
  getKanjiAndWordsFromText,
  addText,
  flushReader,
})(ReadingIndex);
