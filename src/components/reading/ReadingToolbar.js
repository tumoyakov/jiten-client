import React, { useState } from 'react';
import { Paper, Button, makeStyles } from '@material-ui/core';
import { FolderOpen } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';

import AddTextDialog from './AddTextDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    boxSizing: 'border-box',
    height: '56px',
    padding: theme.spacing(1),
  },
  paper: {
    display: 'flex',
    flexDirection: 'row',
    boxSizing: 'border-box',
    padding: theme.spacing(1),
  },
}));

const ReadingToolbar = (props) => {
  const classes = useStyles();

  const { t } = useTranslation();
  const locale = t('ReadingToolbar', { returnObjects: true });

  const { addText } = props;

  const [openDialog, setDialogOpen] = useState(false);

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Button
          variant="outlined"
          startIcon={<FolderOpen />}
          className={classes.menuButton}
          onClick={() => {
            setDialogOpen(true);
          }}>
          {locale.open}
        </Button>
      </Paper>
      <AddTextDialog
        open={openDialog}
        addText={addText}
        handleClose={handleCloseDialog}
      />
    </div>
  );
};

export default ReadingToolbar;
