import React, { useState, Fragment } from 'react';
import clsx from 'clsx';
import { Paper, Button, makeStyles, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import ReaderWordPopover from './ReaderWordPopover';

const useStyles = makeStyles((theme) => ({
  root: {
    boxSizing: 'border-box',
    padding: theme.spacing(1),
  },
  paper: {
    display: 'flex',
    flexDirection: 'row',
    boxSizing: 'border-box',
    padding: theme.spacing(1),
  },
  firstView: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  word: {
    cursor: 'pointer',
  },
}));

const TextReader = (props) => {
  const classes = useStyles();

  const { t } = useTranslation();
  const locale = t('TextReader', { returnObjects: true });

  const { text, data } = props;

  const [currentData, setCurrentData] = useState(null);
  const [currentAnchorEl, setCurrentAnchorEl] = useState(null);

  function restoreTextFromData() {
    let result = [];
    let restoringText = text;
    for (let item of data) {
      const searchedWord = item.word.surface_form;
      const index = restoringText.indexOf(searchedWord);
      if (index !== -1) {
        const findedWord = (
          <span
            className={classes.word}
            onClick={(e) => {
              setCurrentData(item);
              setCurrentAnchorEl(e.target);
            }}>
            {restoringText.slice(index, index + searchedWord.length)}
          </span>
        );
        const textBefore = (
          <span className={classes.simple}>
            {restoringText.slice(0, index)}
          </span>
        );
        restoringText = restoringText.slice(
          index + searchedWord.length,
          restoringText.length
        );
        result.push(textBefore);
        result.push(findedWord);
      }
    }
    result.push(<span className={classes.simple}>{restoringText}</span>);

    return result;
  }

  function renderText() {
    return <Fragment>{restoreTextFromData()}</Fragment>;
  }

  function renderEmptyViewMessage() {
    return (
      <div className={clsx(classes.root, classes.firstView)}>
        <Typography variant="body2" component="div">
          Для начала чтения добавьте текст
        </Typography>
      </div>
    );
  }

  function handlePopoverClose() {
    setCurrentData(null);
    setCurrentAnchorEl(null);
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        {!text ? (
          renderEmptyViewMessage()
        ) : (
          <Typography variant="body1">{renderText()}</Typography>
        )}
      </Paper>
      <ReaderWordPopover
        currentData={currentData}
        anchorEl={currentAnchorEl}
        handleClose={handlePopoverClose}
      />
    </div>
  );
};

export default TextReader;
