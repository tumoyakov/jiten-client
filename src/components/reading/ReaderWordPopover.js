import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import {
  Popover,
  Tabs,
  Tab,
  Typography,
  Button,
  makeStyles,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import ReadingWordCard from './ReadingWordCard';
import ReadingKanjiCard from './ReadingKanjiCard';
import Loading from '../common/Loading';
import { fetchKanjis, fetchWords, fetchKanji } from '../../actions';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  prompt: {
    boxSizing: 'border-box',
    padding: theme.spacing(1),
    maxWidth: '400px',
    maxHeight: '600px',
  },
  resultList: {
    display: 'flex',
    flexDirection: 'column',
  },
  empty: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxSizing: 'border-box',
    padding: theme.spacing(1),
  },
}));

const ReaderWordPopover = (props) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const locale = t('ReaderWordPopover', { returnObjects: true });

  const {
    currentData,
    kanjis,
    words,
    anchorEl,
    handleClose,
    fetchKanjis,
    fetchWords,
  } = props;

  const [loadingKanjis, setKanjisLoading] = useState(false);
  const [loadingWords, setWordsLoading] = useState(false);

  const [tabValue, setTabValue] = useState(0);
  const handleChangeTabValue = (event, newValue) => {
    setTabValue(newValue);
  };

  const open = Boolean(currentData) && Boolean(anchorEl);

  const word = currentData ? currentData.word : {};

  useEffect(() => {
    if (currentData) {
      if (currentData.kanjis.some((id) => kanjis[id] === undefined)) {
        setKanjisLoading(true);
        fetchKanjis(currentData.kanjis)
          .then(() => {
            setKanjisLoading(false);
          })
          .catch(() => {
            setKanjisLoading(false);
          });
      }
      if (currentData.words.some((id) => words[id] === undefined)) {
        setWordsLoading(true);
        fetchWords(currentData.words)
          .then(() => {
            setWordsLoading(false);
          })
          .catch(() => {
            setWordsLoading(false);
          });
      }
    }
  }, [currentData, kanjis, words]);

  function renderEmptyViewMessage() {
    return (
      <div className={classes.empty}>
        <Typography variant="body2" component="div">
          Нет данных
        </Typography>
      </div>
    );
  }

  function renderWordList() {
    if (loadingWords)
      return (
        <div className={classes.resultList}>
          <Loading key={`word-prompt-loading`} />
        </div>
      );
    return (
      <div className={classes.resultList}>
        {!currentData || currentData.words.length === 0
          ? renderEmptyViewMessage()
          : currentData &&
            currentData.words.map((id) => (
              <ReadingWordCard key={`word-prompt-${id}`} data={words[id]} />
            ))}
      </div>
    );
  }

  function renderKanjiList() {
    if (loadingKanjis)
      return (
        <div className={classes.resultList}>
          <Loading key={`kanji-prompt-loading`} />
        </div>
      );
    return (
      <div className={classes.resultList}>
        {!currentData || currentData.kanjis.length === 0
          ? renderEmptyViewMessage()
          : currentData &&
            currentData.kanjis.map((id) => (
              <ReadingKanjiCard key={`kanji-prompt-${id}`} data={kanjis[id]} />
            ))}
      </div>
    );
  }

  function renderPromptCard() {
    return (
      <div className={classes.prompt}>
        <Tabs
          value={tabValue}
          onChange={handleChangeTabValue}
          indicatorColor="primary"
          textColor="primary">
          <Tab label={locale.word} />
          <Tab label={locale.kanji} />
        </Tabs>
        {tabValue === 0 ? renderWordList() : renderKanjiList()}
      </div>
    );
  }

  return (
    <Popover
      id="reader-word-popover"
      open={open}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}>
      {renderPromptCard()}
    </Popover>
  );
};

const mapStateToProps = (state) => ({
  kanjis: state.kanjis,
  words: state.words,
});

export default connect(mapStateToProps, { fetchKanjis, fetchWords })(
  ReaderWordPopover
);
