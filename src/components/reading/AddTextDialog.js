import React, { useState } from 'react';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const AddTextDialog = (props) => {
  const classes = useStyles();
  const { open, addText, handleClose } = props;
  const { t } = useTranslation();
  const locale = t('AddTextDialog', { returnObjects: true });

  const [value, setValue] = useState('');
  const [error, setError] = useState('');

  const submitText = () => {
    if (!value.trim()) {
      setError(locale.empty);
    } else {
      addText(value.trim());
      setValue('');
      handleClose();
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      className={classes.root}>
      <DialogTitle id="form-dialog-title">{locale.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{locale.description}</DialogContentText>
        <TextField
          multiline
          rows={10}
          autoFocus
          margin="dense"
          value={value}
          onChange={(event) => {
            setValue(event.target.value);
            if (event.target.value.length >= 2000) {
              setError(locale.limit);
            }
          }}
          error={Boolean(error)}
          helperText={error}
          fullWidth
          inputProps={{ maxLength: 3000 }}
          variant="outlined"
        />
      </DialogContent>
      <DialogActions>
        <Typography variant="caption">{locale.limit}</Typography>
        <Button
          onClick={() => {
            setValue('');
            handleClose();
          }}
          color="primary">
          {locale.cancel}
        </Button>
        <Button onClick={submitText} color="primary">
          {locale.add}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddTextDialog;
