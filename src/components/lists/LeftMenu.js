import React from 'react';
import { Link } from 'react-router-dom';
import {
    Paper,
    MenuList,
    MenuItem,
    BottomNavigation,
    BottomNavigationAction,
    makeStyles,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useMobileView } from '../../decorators';
import history from '../../history';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(1),
        width: '30%',
        [theme.downMediaQuery]: {
            position: 'fixed',
            bottom: 0,
            width: '100%',
        },
    },
    paper: {
        marginRight: theme.spacing(2),
    },
    link: {
        color: theme.palette.text.primary,
        textDecoration: 'none',
    },
}));

const LeftMenu = (props) => {
    const classes = useStyles();
    const { t } = useTranslation();
    const isMobile = useMobileView();

    const locale = t('listsLeftMenu', { returnObjects: true });

    return !isMobile ? (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <MenuList>
                    <Link to="/lists/" className={classes.link}>
                        <MenuItem>{locale.kanji}</MenuItem>
                    </Link>
                    <Link to="/lists/words" className={classes.link}>
                        <MenuItem>{locale.word}</MenuItem>
                    </Link>
                </MenuList>
            </Paper>
        </div>
    ) : null;
};

export default LeftMenu;
