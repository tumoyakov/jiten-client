import React from 'react';
import {
    Typography,
    makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%'
    },
    item: {
        width: '100%',
        marginBottom: theme.spacing(1),
    },
    itemText: {
        fontWeight: '400'
    }
}));

const KanjiData = props => {
    const classes = useStyles();
    const { meanings, readings, locale } = props;

    const renderMeanings = () => {
        const result = meanings || meanings.length !== 0 ? (
            meanings.reduce((acc, cur) => {return acc !== '' ? `${acc}, ${cur.meaning}` : cur.meaning} , '')
            ) : (
                locale.meaningsEmpty
            );

        return (
            <div className={classes.item}>
                <Typography variant="h6" className={classes.itemText}>
                    {result}
                </Typography>
            </div>
        );
    }

    const renderReadings = () => {
        const on = readings ? readings.filter(item => item.type === 'ja_on') : [];
        const kun = readings ? readings.filter(item => item.type === 'ja_kun') : [];

        const resultOn = on.length > 0 ? (
            on.reduce((acc, cur) => {
                return acc !== '' ? `${acc}, ${cur.reading}`: cur.reading
            }, '')
        ) : (
            locale.onReadingEmpty
        );

        const resultKun = kun.length > 0 ? (
            kun.reduce((acc, cur) => {
                return acc !== '' ? `${acc}, ${cur.reading}`: cur.reading
            }, '')
        ) : (
            locale.kunReadingEmpty
        );

        return (
            <>
                <div className={classes.item}>
                    <Typography variant="subtitle1" className={classes.itemText}>
                        {locale.onReading}: {resultOn}
                    </Typography>
                </div>
                <div className={classes.item}>
                    <Typography variant="subtitle1" className={classes.itemText}>
                        {locale.kunReading}: {resultKun}
                    </Typography>
                </div>
            </>
        );
    }

    return (
       <div className={classes.root}>
           {renderMeanings()}
           {renderReadings()}
       </div>
    );
};

export default KanjiData;
