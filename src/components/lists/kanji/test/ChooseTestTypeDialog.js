import React from 'react';
import history from '../../../../history';
import {
    Button,
    TextField,
    ListItem,
    ListItemText,
    List,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    makeStyles,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { setListToTest } from '../../../../actions';

const ChooseTestTypeDialog = (props) => {
    const { open, handleClose, items, type, setListToTest } = props;
    const { t } = useTranslation();
    const locale = t('ChooseTestTypeDialog', { returnObjects: true });

    const list = [locale.flashcard, locale.multiple];

    const renderListItem = (
        item,
        index //TODO fix duplicated key
    ) => (
        <ListItem
            key={index}
            button
            divider
            role="listitem"
            onClick={() => handleChoose(index)}>
            <ListItemText primary={item} />
        </ListItem>
    );

    const handleChoose = (index) => {
        setListToTest(items, { entity: type, index });
        history.push('/test');
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{locale.title}</DialogTitle>
            <DialogContent>
                <List component="div" role="list">
                    {list.map((item, index) => renderListItem(item, index))}
                </List>
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={() => {
                        handleClose();
                    }}
                    color="primary">
                    {locale.cancel}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default connect(null, { setListToTest })(ChooseTestTypeDialog);
