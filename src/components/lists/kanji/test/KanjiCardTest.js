import React, { useState } from 'react';
import {
  makeStyles,
  IconButton,
  Paper,
  Typography,
  Button,
} from '@material-ui/core';
import {
  ArrowBackIosRounded,
  ArrowForwardIosRounded,
  VisibilityOff,
  Visibility,
  CloseRounded,
} from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import history from '../../../../history';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
  },
  backBtn: {
    position: 'fixed',
    left: '5%',
    top: '50%',
    transform: 'translateY(-50%)',
    zIndex: 2,
  },
  forwardBtn: {
    position: 'fixed',
    right: '5%',
    top: '50%',
    transform: 'translateY(-50%)',
    zIndex: 2,
  },
  content: {
    width: '400px',
    position: 'fixed',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: '100%',
    maxHeight: '100%',
    zIndex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  literal: {
    margin: theme.spacing(2),
  },
  hidden: { margin: theme.spacing(1) },
  data: {
    flex: '0 0 auto',
    width: '100%',
    'box-sizing': 'border-box',
    padding: theme.spacing(1),
  },
}));

const KanjiCardTest = (props) => {
  const { list, type } = props;
  const classes = useStyles();
  const elements = type.entity === 'kanji' ? list.kanjis : list.words;

  const [curElem, setCurElem] = useState(elements[0]);
  const [enabledBackBtn, setBackBtnEnabled] = useState(false);
  const [enabledForwardBtn, setForwardBtnEnabled] = useState(
    elements.length > 1
  );
  const [curIndex, setCurIndex] = useState(0);
  const [show, setShow] = useState(false);

  function handleBackClick() {
    setShow(false);
    const newIndex = curIndex - 1;
    setCurElem(elements[newIndex]);
    setCurIndex(newIndex);
    if (newIndex === 0) setBackBtnEnabled(false);
    if (newIndex === elements.length - 2) setForwardBtnEnabled(true);
  }

  function handleForwardClick() {
    setShow(false);
    const newIndex = curIndex + 1;
    setCurElem(elements[newIndex]);
    setCurIndex(newIndex);
    if (newIndex === 1) setBackBtnEnabled(true);
    if (newIndex === elements.length - 1) setForwardBtnEnabled(false);
  }

  function handleClose() {
    const to = `/${type.entity}/list/${list.id}`;
    history.push(to);
  }

  const { t, i18n } = useTranslation();
  const locale = t('KanjiFlashCard', { returnObjects: true });

  //TODO move to component
  function getReadings(type) {
    const on = curElem.kanjiReadings
      ? curElem.kanjiReadings.filter((item) => item.type === type) //['ja_on','ja_kun']
      : [];

    return on.length > 0
      ? on.reduce((acc, cur) => {
          return acc !== '' ? `${acc}, ${cur.reading}` : cur.reading;
        }, '')
      : type === 'ja_on'
      ? locale.onReadingEmpty
      : locale.kunReadingEmpty;
  }

  //TODO move to component
  function getMeanings() {
    const langMeanings =
      Boolean(curElem.kanjiMeanings) && i18n.language !== 'en'
        ? curElem.kanjiMeanings.filter((item) => {
            return item.lang === 'ru' || item.lang === 'rus';
          })
        : null;

    const meanings =
      Boolean(langMeanings) && langMeanings.length !== 0
        ? langMeanings
        : curElem.kanjiMeanings
        ? curElem.kanjiMeanings.filter((item) => {
            return item.lang === 'en' || item.lang === 'eng';
          })
        : [];

    return meanings.length > 0
      ? meanings.reduce((acc, cur) => {
          return acc ? `${acc}, ${cur.meaning}` : cur.meaning;
        }, '')
      : locale.meaningsEmpty;
  }

  return (
    <div className={classes.root}>
      {enabledBackBtn ? (
        <IconButton
          color="primary"
          onClick={handleBackClick}
          className={classes.backBtn}>
          <ArrowBackIosRounded />
        </IconButton>
      ) : null}
      <Paper className={classes.content}>
        <Typography variant="h1" className={classes.literal}>
          {curElem.literal}
        </Typography>
        {show ? (
          <div className={classes.data}>
            <Typography variant="body2" component="div">
              {`${locale.on}${getReadings('ja_on')}`}
            </Typography>
            <Typography variant="body2" component="div">
              {`${locale.kun}${getReadings('ja_kun')}`}
            </Typography>
            <Typography variant="body2" component="div">
              {`${locale.meanings}${getMeanings()}`}
            </Typography>
          </div>
        ) : null}
        <Button
          variant="outlined"
          endIcon={show ? <VisibilityOff /> : <Visibility />}
          onClick={() => setShow(!show)}
          className={classes.hidden}>
          {show ? locale.hide : locale.show}
        </Button>
      </Paper>
      {enabledForwardBtn ? (
        <IconButton
          color="primary"
          onClick={handleForwardClick}
          className={classes.forwardBtn}>
          <ArrowForwardIosRounded />
        </IconButton>
      ) : null}
      {!enabledForwardBtn ? (
        <IconButton
          color="primary"
          onClick={handleClose}
          className={classes.forwardBtn}>
          <CloseRounded />
        </IconButton>
      ) : null}
    </div>
  );
};

export default KanjiCardTest;
