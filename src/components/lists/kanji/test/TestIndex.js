import React from 'react';
import history from '../../../../history';
import { Switch, Route, useRouteMatch, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import KanjiCardTest from './KanjiCardTest';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    [theme.downMediaQuery]: {
      flexDirection: 'column',
    },
  },
  content: {
    width: '100%',
    minHeight: '100%',
  },
}));

const TestIndex = (props) => {
  const classes = useStyles();

  const { list, type } = props;

  if (!list) history.push('/lists');

  console.log(list, type);

  const renderTest = () =>
    type && type.index === 0 ? <KanjiCardTest list={list} type={type} /> : null;
  return (
    <div className={classes.root}>
      <div className={classes.content}>{renderTest()}</div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.test.list,
    type: state.test.type,
  };
};

export default connect(mapStateToProps, {})(TestIndex);
