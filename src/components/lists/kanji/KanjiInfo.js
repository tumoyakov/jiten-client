import React from 'react';
import {
    Chip,
    makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(1),
        width: '100%'
    },
    item: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    chip: {
        width: '100%'
    }
}));

const KanjiInfo = props => {
    const classes = useStyles();
    const { jlpt, strokeCount, locale } = props;

    return (
        <div className={classes.root}>
            { jlpt ? (
                <div className={classes.item}>
                        <Chip 
                            className={classes.chip}
                            variant="outlined"
                            size="small"
                            color="primary"
                            label={`${locale.jlpt}${jlpt}`}
                        />
                </div>
            ) : null }
            { strokeCount ? (
                <div className={classes.item}>
                        <Chip 
                            className={classes.chip}
                            variant="outlined"
                            size="small"
                            color="primary"
                            label={`${locale.strokeCount}${strokeCount}`}
                        />
                </div>
            ) : null }
        </div>
    );
};

export default KanjiInfo;
