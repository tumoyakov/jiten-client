import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  Divider,
  makeStyles,
  Typography,
  Paper,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormControlLabel,
  Button,
  Switch,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import KanjiCard from './KanjiCard';
import ChooseTestTypeDialog from '../test/ChooseTestTypeDialog';
import Loading from '../../../common/Loading';
import {
  getKanjiList,
  deleteKanjiFromList,
  changePageInKanjiList,
} from '../../../../actions';
import { calculateCountByLength } from '../../../../reducers/utils';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    [theme.downMediaQuery]: {
      flexDirection: 'column',
    },
  },
  loading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    marginRight: theme.spacing(1),
  },
  pagination: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(1),
    width: '100%',
  },
  menu: {
    width: '25%',
    height: 'fit-content',
    marginTop: theme.spacing(1),
    [theme.downMediaQuery]: {
      width: '100%',
      marginTop: 0,
    },
  },
  menuContent: {
    margin: theme.spacing(1),
  },
  content: {
    width: '70%',
    [theme.downMediaQuery]: {
      width: '100%',
      marginTop: theme.spacing(1),
    },
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
    width: '100%',
  },
  marginTop: {
    marginTop: theme.spacing(1),
  },
  testButtom: {
    marginTop: theme.spacing(1),
  },
  checkBoxLabel: {
    margin: 0,
    marginTop: theme.spacing(1),
  },
}));

const jlptFields = ['all', 'JLPT 1', 'JLPT 2', 'JLPT 3', 'JLPT 4', 'JLPT 5'];

const prepareListToTest = (list, kanjis) => {
  return { ...list, kanjis };
};

const KanjiList = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [jlpt, setJlpt] = useState(0);
  const [multiple, setMultiple] = useState(false);
  const [openTestDialog, setTestDialogOpen] = useState(false);
  const id = parseInt(props.match.params.id);
  const { t } = useTranslation();
  const locale = t('KanjiList', { returnObjects: true });

  const {
    kanjiList,
    kanjis,
    getKanjiList,
    deleteKanjiFromList,
    changePageInKanjiList,
  } = props;

  useEffect(() => {
    if (!kanjiList || !kanjis || kanjiList.kanjis.length !== kanjis.length) {
      setLoading(true);
      getKanjiList(id)
        .then(() => {
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        });
    }
  }, []);

  if (loading || !kanjiList) {
    return (
      <div className={classes.root}>
        <Loading />
      </div>
    );
  }

  if (!kanjis || kanjis.length === 0) {
    console.log('no kanjis');
    return (
      <div className={classes.root}>
        <div className={classes.loading}>
          <Typography variant="subtitle1">{locale.empty}</Typography>
        </div>
      </div>
    );
  }

  const filteredKanjis = kanjis.filter((item) =>
    jlpt > 0 ? item.jlpt === jlpt : true
  );

  const { list, page } = kanjiList;

  const pageCount = calculateCountByLength(filteredKanjis.length);

  const handleDelete = (kanjiId) => {
    deleteKanjiFromList(kanjiId, id);
  };

  const renderList = (item) => (
    <KanjiCard key={item.id} data={item} handleDelete={handleDelete} />
  );

  const handlePageChange = (event, page) => {
    changePageInKanjiList(list.id, page);
  };

  const handleChangeJLPTSelect = (event) => {
    setJlpt(event.target.value);
  };

  const renderMenu = () => {
    return (
      <React.Fragment>
        <Divider />
        <FormControl className={classes.formControl}>
          <InputLabel id="jlpt-select-label">{locale.jlptFilter}</InputLabel>
          <Select
            labelId="jlpt-select-label"
            id="jlpt-select"
            value={jlpt}
            onChange={handleChangeJLPTSelect}>
            {jlptFields.map((item, index) => (
              <MenuItem value={index}>{item}</MenuItem>
            ))}
          </Select>
          <FormControlLabel
            className={classes.checkBoxLabel}
            control={
              <Switch
                checked={multiple}
                onChange={() => setMultiple(!multiple)}
                color="primary"
              />
            }
            label={locale.multiple}
            labelPlacement="end"
          />
          <Button
            className={classes.testButtom}
            variant="outlined"
            color="primary"
            onClick={() => setTestDialogOpen(true)}>
            {locale.test}
          </Button>
          <ChooseTestTypeDialog
            open={openTestDialog}
            items={prepareListToTest(list, filteredKanjis)}
            type="kanji"
            handleClose={() => setTestDialogOpen(false)}
          />
        </FormControl>
      </React.Fragment>
    );
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.menu}>
        <div className={classes.menuContent}>
          <Typography variant="h6">{list.name}</Typography>
          {renderMenu()}
        </div>
      </Paper>
      <div className={classes.content}>
        {filteredKanjis
          .slice((page - 1) * 10, page * 10)
          .map((item, index) => renderList(item, index))}
        <div className={classes.pagination}>
          {pageCount > 0 ? (
            <Pagination
              page={page}
              count={pageCount}
              onChange={handlePageChange}
              showFirstButton
              showLastButton
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const kanjiList = state.kanjiLists.lists[parseInt(ownProps.match.params.id)];
  const kanjis =
    kanjiList && kanjiList.list
      ? Object.values(state.kanjis).filter((item) => {
          return (
            kanjiList.kanjis &&
            kanjiList.kanjis.find((kanji) => kanji === item.id)
          );
        })
      : [];
  return {
    kanjiList,
    kanjis,
  };
};

export default connect(mapStateToProps, {
  getKanjiList,
  changePageInKanjiList,
  deleteKanjiFromList,
})(KanjiList);
