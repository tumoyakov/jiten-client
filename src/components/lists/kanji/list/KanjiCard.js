import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Card, Button, Typography, makeStyles } from '@material-ui/core';
import KanjiInfo from './KanjiInfo';
import KanjiData from './KanjiData';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'row',
  },
  leftSide: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(1),
  },
  rightSide: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  menuButton: {
    textDecoration: 'none',
    marginRight: theme.spacing(1),
  },
}));

const KanjiCard = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  const { data, handleDelete } = props;
  if (!data) return null;

  const langMeanings =
    Boolean(data.kanjiMeanings) && i18n.language !== 'en'
      ? data.kanjiMeanings.filter((item) => {
          return item.lang === 'ru' || item.lang === 'rus';
        })
      : null;

  const meanings =
    Boolean(langMeanings) && langMeanings.length !== 0
      ? langMeanings
      : data.kanjiMeanings
      ? data.kanjiMeanings.filter((item) => {
          return item.lang === 'en' || item.lang === 'eng';
        })
      : [];

  const readings = data.kanjiReadings ? data.kanjiReadings : [];
  const locale = t('card', { returnObjects: true });

  const renderBottomCardMenu = () => {
    return (
      <div className={classes.menu}>
        <Link to={`/kanji/${data.id}`} className={classes.menuButton}>
          <Button variant="outlined" size="small" color="primary">
            {locale.detail}
          </Button>
        </Link>
        <Button
          variant="outlined"
          size="small"
          color="primary"
          onClick={() => handleDelete(data.id)}>
          {locale.delete}
        </Button>
      </div>
    );
  };

  return (
    <Card className={classes.root}>
      <div className={classes.cardContent}>
        <div className={classes.leftSide}>
          <Typography variant="h2" component="div">
            {data.literal}
          </Typography>
          <KanjiInfo
            jlpt={data.jlpt}
            strokeCount={data.strokeCount}
            locale={locale}
          />
        </div>
        <div className={classes.rightSide}>
          <KanjiData meanings={meanings} readings={readings} locale={locale} />
          {renderBottomCardMenu()}
        </div>
      </div>
    </Card>
  );
};

export default KanjiCard;
