import React from 'react';
import { useTranslation } from 'react-i18next';
import { Card, Button, makeStyles } from '@material-ui/core';
import WordData from './WordData';
import { Link } from 'react-router-dom';
import { kElementsToString, rElementsToString } from '../../../words/WordUtils';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  menuButton: {
    textDecoration: 'none',
    marginRight: theme.spacing(1),
  },
}));

const WordCard = (props) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  const { data, handleDelete } = props;

  if (!data) return null;

  const keb = kElementsToString(data.kanjiElements);
  const reb = rElementsToString(data.readElements);

  const locale = t('card', { returnObjects: true });

  const renderBottomCardMenu = () => {
    return (
      <div className={classes.menu}>
        <Link to={`/word/${data.id}`} className={classes.menuButton}>
          <Button variant="outlined" size="small" color="primary">
            {locale.detail}
          </Button>
        </Link>
        <Button
          variant="outlined"
          size="small"
          color="primary"
          onClick={() => handleDelete(data.id)}>
          {locale.delete}
        </Button>
      </div>
    );
  };

  return (
    <Card className={classes.root}>
      <div className={classes.cardContent}>
        <WordData keb={keb} reb={reb} senses={data.senses} locale={locale} />
        {renderBottomCardMenu()}
      </div>
    </Card>
  );
};

export default WordCard;
