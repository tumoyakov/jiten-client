import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  Divider,
  makeStyles,
  Typography,
  Paper,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormControlLabel,
  Button,
  Switch,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import WordCard from './WordCard';
//import ChooseTestTypeDialog from '../test/ChooseTestTypeDialog';
import Loading from '../../../common/Loading';
import {
  getWordList,
  deleteWordFromList,
  changePageInWordList,
} from '../../../../actions';
import { calculateCountByLength } from '../../../../reducers/utils';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    [theme.downMediaQuery]: {
      flexDirection: 'column',
    },
  },
  loading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    marginRight: theme.spacing(1),
  },
  pagination: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(1),
    width: '100%',
  },
  menu: {
    width: '25%',
    height: 'fit-content',
    marginTop: theme.spacing(1),
    [theme.downMediaQuery]: {
      width: '100%',
      marginTop: 0,
    },
  },
  menuContent: {
    margin: theme.spacing(1),
  },
  content: {
    width: '70%',
    [theme.downMediaQuery]: {
      width: '100%',
      marginTop: theme.spacing(1),
    },
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
    width: '100%',
  },
  marginTop: {
    marginTop: theme.spacing(1),
  },
  testButton: {
    marginTop: theme.spacing(2),
    minWidth: 120,
    width: '100%',
  },
  checkBoxLabel: {
    margin: 0,
    marginTop: theme.spacing(1),
  },
}));

const prepareListToTest = (list, words) => {
  return { ...list, words };
};

const WordList = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [openTestDialog, setTestDialogOpen] = useState(false);
  const id = parseInt(props.match.params.id);
  const { t } = useTranslation();
  const locale = t('KanjiList', { returnObjects: true });

  const {
    wordList,
    words,
    getWordList,
    deleteWordFromList,
    changePageInWordList,
  } = props;

  useEffect(() => {
    if (!wordList || !words || wordList.words.length !== words.length) {
      setLoading(true);
      getWordList(id)
        .then(() => {
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        });
    }
  }, []);

  if (loading || !wordList) {
    return (
      <div className={classes.root}>
        <Loading />
      </div>
    );
  }

  if (!words || words.length === 0) {
    return (
      <div className={classes.root}>
        <div className={classes.loading}>
          <Typography variant="subtitle1">{locale.empty}</Typography>
        </div>
      </div>
    );
  }

  const { list, page } = wordList;

  const pageCount = calculateCountByLength(words.length);

  const handleDelete = (wordId) => {
    deleteWordFromList(wordId, id);
  };

  const renderList = (item) => (
    <WordCard key={item.id} data={item} handleDelete={handleDelete} />
  );

  const handlePageChange = (event, page) => {
    changePageInWordList(list.id, page);
  };

  const renderMenu = () => {
    return (
      <React.Fragment>
        <Divider />
        <Button
          className={classes.testButton}
          variant="outlined"
          color="primary"
          onClick={() => setTestDialogOpen(true)}>
          {locale.test}
        </Button>
      </React.Fragment>
    );
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.menu}>
        <div className={classes.menuContent}>
          <Typography variant="h6">{list.name}</Typography>
          {renderMenu()}
        </div>
      </Paper>
      <div className={classes.content}>
        {words
          .slice((page - 1) * 10, page * 10)
          .map((item, index) => renderList(item, index))}
        <div className={classes.pagination}>
          {pageCount > 0 ? (
            <Pagination
              page={page}
              count={pageCount}
              onChange={handlePageChange}
              showFirstButton
              showLastButton
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const wordList = state.wordLists.lists[parseInt(ownProps.match.params.id)];

  const words =
    wordList && wordList.list
      ? Object.values(state.words).filter((item) => {
          return (
            wordList.words && wordList.words.find((word) => word === item.id)
          );
        })
      : [];
  return {
    wordList,
    words,
  };
};

export default connect(mapStateToProps, {
  getWordList,
  changePageInWordList,
  deleteWordFromList,
})(WordList);