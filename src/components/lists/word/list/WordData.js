import React, { Fragment } from 'react';
import { Typography, Divider, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  item: {
    width: '100%',
    marginBottom: theme.spacing(1),
  },
  itemText: {
    fontWeight: '400',
  },
}));

const WordData = (props) => {
  const classes = useStyles();
  const { keb, reb, senses } = props;

  const renderKanjiElements = () => {
    return keb ? (
      <div className={classes.item}>
        <Typography variant="h6" className={classes.itemText}>
          {keb}
        </Typography>
      </div>
    ) : null;
  };

  const renderReadings = () => {
    return reb ? (
      <div className={classes.item}>
        {keb ? (
          <Typography variant="subtitle2" className={classes.itemText}>
            {reb}
          </Typography>
        ) : (
          <Typography variant="h6" className={classes.itemText}>
            {reb}
          </Typography>
        )}
      </div>
    ) : null;
  };

  const renderSenses = () => {
    if (!senses || senses.length === 0) return null;
    let allGlosses = [];
    senses.forEach((sense) => {
      sense.glosses &&
        sense.glosses
          .filter((gloss) => gloss.lang === 'eng' || gloss.lang === 'rus')
          .forEach((gloss) => allGlosses.push(gloss));
    });

    const engGlosses = allGlosses.filter((gloss) => gloss.lang === 'eng');
    const rusGlosses = allGlosses.filter((gloss) => gloss.lang === 'rus');

    return (
      <div className={classes.item}>
        {engGlosses && engGlosses.length > 0 ? (
          <Fragment>
            <Typography variant="subtitle2" className={classes.itemText}>
              eng
            </Typography>
            <Divider />
            {engGlosses.map((gloss, index) => renderGloss(gloss, index))}
          </Fragment>
        ) : null}
        {rusGlosses && rusGlosses.length > 0 ? (
          <Fragment>
            <Typography variant="subtitle2" className={classes.itemText}>
              rus
            </Typography>
            <Divider />
            {rusGlosses.map((gloss, index) => renderGloss(gloss, index))}
          </Fragment>
        ) : null}
      </div>
    );
  };

  const renderGloss = (gloss, index) => (
    <Typography variant="subtitle2" className={classes.itemText}>
      {`${index + 1}) ${gloss.data} ( ${gloss.source} )`}
    </Typography>
  );

  return (
    <div className={classes.root}>
      {renderKanjiElements()}
      {renderReadings()}
      {renderSenses()}
    </div>
  );
};

export default WordData;
