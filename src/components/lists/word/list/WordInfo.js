import React from 'react';
import { Chip, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  chip: {
    width: '100%',
  },
}));

const WordInfo = (props) => {
  const classes = useStyles();
  const { locale } = props;

  return (
    <div className={classes.root}>
      <div className={classes.item}>
        <Chip
          className={classes.chip}
          variant="outlined"
          size="small"
          color="primary"
          label={`${locale.jlpt} mada mada desu`}
        />
      </div>
    </div>
  );
};

export default WordInfo;
