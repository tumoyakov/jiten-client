import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import WordListCard from './WordListCard';
import WordListMenu from './WordListsMenu';
import Loading from '../../common/Loading';
import { getWordLists, changePageWordList } from '../../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
  },
  loading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    marginRight: theme.spacing(1),
  },
  pagination: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(1),
    width: '100%',
  },
}));

const WordListsIndex = (props) => {
  const classes = useStyles();
  const [changeMode, setChangeMode] = useState(false);
  const [loading, setLoading] = useState(false);
  const { lists, page, count, getWordLists, changePageWordList } = props;

  useEffect(() => {
    setLoading(true);
    getWordLists()
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  const renderList = (item, index) => (
    <WordListCard
      key={item.list.id}
      data={item.list}
      index={index}
      count={item.count}
      changeMode={changeMode}
    />
  );

  const handleChangeMode = () => {
    setChangeMode(!changeMode);
  };

  const handlePageChange = (event, page) => {
    changePageWordList(page);
  };

  return (
    <div className={classes.root}>
      <WordListMenu handleChangeMode={handleChangeMode} />
      {loading || !lists ? (
        <Loading />
      ) : (
        lists
          .slice((page - 1) * 10, page * 10)
          .map((item, index) => renderList(item, index))
      )}
      <div className={classes.pagination}>
        {count > 0 ? (
          <Pagination
            page={page}
            count={count}
            onChange={handlePageChange}
            showFirstButton
            showLastButton
          />
        ) : null}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    lists: Object.values(state.wordLists.lists),
    page: state.wordLists.page,
    count: state.wordLists.count,
  };
};

export default connect(mapStateToProps, { getWordLists, changePageWordList })(
  WordListsIndex
);
