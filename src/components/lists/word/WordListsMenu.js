import React, { useState } from 'react';
import { Paper, Button, IconButton, makeStyles } from '@material-ui/core';
import { CreateOutlined, AddCircleOutlined } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { useMobileView } from '../../../decorators';
import AddListDialog from '../../common/dialogs/AddListDialog';
import { connect } from 'react-redux';
import { addWordList } from '../../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(2),
  },
  menuButton: {
    marginRight: theme.spacing(1),
  },
}));

const WordListsMenu = (props) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const locale = t('KanjiListsMenu', { returnObjects: true });
  const isMobile = useMobileView();

  const { handleChangeMode, addWordList } = props;

  const [openAddDialog, setAddDialogOpen] = useState(false);

  const handleAppDialogClose = () => {
    setAddDialogOpen(false);
  };

  const handleListCreate = (name) => {
    addWordList(name);
  };

  const renderButton = (icon, label, onClick) =>
    isMobile ? (
      <IconButton className={classes.menuButton} onClick={onClick}>
        {icon}
      </IconButton>
    ) : (
      <Button startIcon={icon} className={classes.menuButton} onClick={onClick}>
        {label}
      </Button>
    );

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        {renderButton(<AddCircleOutlined />, locale.add, () =>
          setAddDialogOpen(true)
        )}
        {renderButton(<CreateOutlined />, locale.changeMode, handleChangeMode)}
      </Paper>
      <AddListDialog
        open={openAddDialog}
        handleClose={handleAppDialogClose}
        handleListCreate={handleListCreate}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    lists: state.wordLists,
  };
};

export default connect(mapStateToProps, { addWordList })(WordListsMenu);
