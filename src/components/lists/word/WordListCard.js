import React, { useState } from 'react';
import history from '../../../history';
import { Card, IconButton, Typography, makeStyles } from '@material-ui/core';
import { CreateOutlined, DeleteOutlined } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import ChangeListDialog from '../../common/dialogs/ChangeListDialog';
import { connect } from 'react-redux';
import { updateWordList, deleteWordList } from '../../../actions';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: '56px',
    padding: theme.spacing(1),
  },
  data: {
    paddingLeft: theme.spacing(2),
    color: theme.palette.text.primary,
    textDecoration: 'none',
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingRight: theme.spacing(2),
  },
  menuButton: {
    textDecoration: 'none',
  },
}));

const WordListCard = (props) => {
  const classes = useStyles();

  const { t } = useTranslation();

  const locale = t('WordListCard', { returnObjects: true });

  const {
    data,
    count,
    index,
    changeMode,
    updateWordList,
    deleteWordList,
  } = props;

  const [openChangeDialog, setChangeDialogOpen] = useState(false);

  const handleDialogClose = (e) => {
    setChangeDialogOpen(false);
  };

  const handleListChange = (name) => {
    updateWordList({ ...data, name });
  };

  const handleGoToWord = (id) => {
    history.push(`/word/list/${id}`);
  };

  if (!data) return null;

  const renderChangeMenu = () => {
    return changeMode ? (
      <div className={classes.menu}>
        <IconButton color="inherit" onClick={() => setChangeDialogOpen(true)}>
          <CreateOutlined />
        </IconButton>
        <IconButton color="inherit" onClick={() => deleteWordList(data)}>
          <DeleteOutlined />
        </IconButton>
        <ChangeListDialog
          defaultValue={data.name}
          open={openChangeDialog}
          handleClose={handleDialogClose}
          handleSubmit={handleListChange}
        />
      </div>
    ) : null;
  };

  return (
    <Card className={classes.root}>
      <div className={classes.cardContent}>
        <div className={classes.data} onClick={() => handleGoToWord(data.id)}>
          <Typography variant="h6" component="div">
            {data.name}
          </Typography>
          <Typography variant="body2" component="div">
            {`${locale.count}${count}`}
          </Typography>
        </div>
        {renderChangeMenu()}
      </div>
    </Card>
  );
};

export default connect(null, { updateWordList, deleteWordList })(WordListCard);
