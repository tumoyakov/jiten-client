import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import LeftMenu from './LeftMenu';
import KanjiListsIndex from './kanji/KanjiListsIndex';
import WordListsIndex from './word/WordListsIndex';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(1),
    boxSizing: 'border-box',
    [theme.downMediaQuery]: {
      padding: 0,
      flexDirection: 'column',
    },
  },
  content: {
    width: '70%',
    minHeight: '100%',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.downMediaQuery]: {
      width: '100%',
      flexDirection: 'column',
      margin: 0,
    },
  },
}));

const ListsIndex = (props) => {
  const classes = useStyles();

  let { path: curPath, url } = useRouteMatch();

  return (
    <div className={classes.root}>
      <LeftMenu />
      <div className={classes.content}>
        <Switch>
          <Route path={`${curPath}`} exact component={KanjiListsIndex} />
          <Route path={`${curPath}/words`} exact component={WordListsIndex} />
        </Switch>
      </div>
    </div>
  );
};

export default ListsIndex;
