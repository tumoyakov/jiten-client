import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProtectedRoute from '../common/ProtectedRoute';
import Search from '../search/Search';
import KanjiDetail from '../kanjis/KanjiDetail';
import WordDetail from '../words/WordDetail';
import UserCabinet from '../cabinet/UserCabinet';
import Login from '../cabinet/Login';
import Registration from '../cabinet/Registration';
import Informational from '../common/Informational';
import ListsIndex from '../lists/ListsIndex';
import KanjiList from '../lists/kanji/list/KanjiList';
import TestIndex from '../lists/kanji/test/TestIndex';
import WordList from '../lists/word/list/WordList';
import ReadingIndex from '../reading/ReadingIndex';
import About from '../About';

const AppSwitch = () => {
  return (
    <Switch>
      <Route path="/" exact component={Search} />
      <Route path="/login" exact component={Login} />
      <Route path="/registration" exact component={Registration} />
      <Route path="/kanji/:id" exact component={KanjiDetail} />
      <Route path="/word/:id" exact component={WordDetail} />
      <Route path="/reading" exact component={ReadingIndex} />
      <ProtectedRoute path="/cabinet" component={UserCabinet} />
      <ProtectedRoute path="/lists" component={ListsIndex} />
      <ProtectedRoute path="/kanji/list/:id" exact component={KanjiList} />
      <ProtectedRoute path="/word/list/:id" exact component={WordList} />
      <ProtectedRoute path="/test" exact component={TestIndex} />
      <Route path="/about" exact component={About} />
      <Route
        path="/403"
        exact
        component={() => (
          <Informational text="У Вас нет доступа к данной странице." />
        )}
      />
      <Route
        component={() => (
          <Informational text="Ошибка 404. Страница не найдена." />
        )}
      />
    </Switch>
  );
};

export default AppSwitch;
