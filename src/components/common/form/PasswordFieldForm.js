import React, { useState } from 'react';
import clsx from 'clsx';
import {
    Input,
    InputAdornment,
    FormControl,
    InputLabel,
    OutlinedInput,
    IconButton,
    makeStyles
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
        formInput: {
            width: '100%',
            marginBottom: theme.spacing(1)
        },
        withoutLabel: {
            marginTop: theme.spacing(3),
        },
        textField: {
            width: '100%',
        },
}));

const PasswordFieldForm = ({
    input,
    label,
    meta: { touched, invalid, error }
}) => {

    const [showPassword, setShowPassword] = useState(false);

    const classes = useStyles();
    return (
        <div className={classes.formInput}>
            <FormControl fullWidth className={clsx(classes.margin, classes.textField)} variant="outlined">
            <InputLabel htmlFor={`filled-adornment-password-${label}`}>{label}</InputLabel>
            <OutlinedInput
                id={`filled-adornment-password-${label}`}
                type={showPassword ? 'text' : 'password'}
                error={touched && invalid}
                helperText={touched && error}
                fullWidth
                {...input}
                endAdornment={
                <InputAdornment position="end">
                    <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPassword(!showPassword)}
                    edge="end"
                    >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                </InputAdornment>
                }
                labelWidth={label.length*9}
            />
            </FormControl>
        </div>
    );
};

export default PasswordFieldForm;