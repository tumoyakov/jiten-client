import React from 'react';
import {
    TextField,
    makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    formInput: {
        width: '100%',
        marginBottom: theme.spacing(1)
    },
    formElement: {
        width: '100%',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    }
}));

const TextFieldForm = ({
    input,
    label,
    meta: { touched, invalid, error },
    ...custom
}) => {
    const classes = useStyles();
    return (
        <div className={classes.formInput}>
            <TextField
                variant="outlined"
                className={classes.formElement}
                label={label}
                error={touched && invalid}
                helperText={touched && error}
                {...input}
                {...custom}
            />
        </div>
    );
};

export default TextFieldForm;