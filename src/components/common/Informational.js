import React, { Component } from 'react';
import { Typography } from '@material-ui/core';
import { Warning } from '@material-ui/icons';

export default class Informational extends Component {
  render() {
    return <div style={{display:'flex', flexDirection: 'column', width: '100%', height: '100%', alignItems: 'center', justifyContent:'center'}}>
      <Warning style={{fontSize: '100px'}} color="disabled" />
      <Typography component="div" style={{padding:16, textAlign: 'center'}} color="textPrimary" variant="h5">{this.props.text}</Typography>
    </div>;
  }
}