import React from "react";
import icon from "../../static/lang/us.svg";
import { SvgIcon } from "@material-ui/core";

const UsIcon = () => {
  return <SvgIcon component={icon} />;
};

export default UsIcon;
