import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import Informational from "./Informational";

function ProtectedRoute(props) {
  const { path, component, auth, ...rest } = props;
  const notAuthorized = () => <Informational text="У Вас нет доступа к данной странице." />;
    return (
        <Route path={path} component={auth ? component : notAuthorized} {...rest} />
    );
}

const mapStateToProps = state => {
  return {
      auth: state.session.auth
  };
}

export default connect(mapStateToProps, {})(ProtectedRoute);