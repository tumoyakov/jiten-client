import React, { useState } from 'react';
import {
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    makeStyles,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const AddListDialog = (props) => {
    const { open, handleListCreate, handleClose } = props;
    const { t } = useTranslation();
    const locale = t('AddListDialog', { returnObjects: true });

    const [value, setValue] = useState('');
    const [error, setError] = useState('');

    const submitList = () => {
        if (!value.trim()) {
            setError(locale.emptyName);
        } else {
            handleListCreate(value);
            setValue('');
            handleClose();
        }
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{locale.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>{locale.describe}</DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    value={value}
                    onChange={(event) => {
                        setValue(event.target.value);
                        setError('');
                    }}
                    error={Boolean(error)}
                    helperText={error}
                    fullWidth
                />
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={() => {
                        setValue('');
                        handleClose();
                    }}
                    color="primary">
                    {locale.cancel}
                </Button>
                <Button onClick={submitList} color="primary">
                    {locale.create}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddListDialog;
