import React, { useState, useEffect } from 'react';
import {
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    makeStyles,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const ChangeListDialog = (props) => {
    const { open, defaultValue, handleSubmit, handleClose } = props;
    const { t } = useTranslation();
    const locale = t('ChangeListDialog', { returnObjects: true });

    const [value, setValue] = useState('');
    const [error, setError] = useState('');

    useEffect(() => {
        setValue(defaultValue);
    }, [defaultValue]);

    const submitList = () => {
        if (!value.trim()) {
            setError(locale.emptyName);
        } else {
            handleSubmit(value);
            handleClose();
        }
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{locale.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>{locale.describe}</DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    value={value}
                    onChange={(event) => {
                        setValue(event.target.value);
                        setError('');
                    }}
                    error={Boolean(error)}
                    helperText={error}
                    fullWidth
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    {locale.cancel}
                </Button>
                <Button onClick={submitList} color="primary">
                    {locale.create}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ChangeListDialog;
