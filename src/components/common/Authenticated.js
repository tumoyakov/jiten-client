import { connect } from 'react-redux';

const Autenticated = props => {
    const { children, defaultResult, session, isAuth } = props;

    if (isAuth && !session.auth) {
        return defaultResult;
    }

    return children;
};
  
Autenticated.defaultProps = {
    defaultResult: null
};

const mapStateToProps = state => {
    return {
        session: state.session
    };
}
  
export default connect(mapStateToProps, {})(Autenticated);
  