import React from 'react';
import history from '../../../history';
import {
    Menu,
    MenuItem,
    ListItemIcon,
    ListItemText,
    makeStyles
} from '@material-ui/core';
import { AccountBox, ExitToApp } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { signout } from '../../../actions';

const useStyles = makeStyles(theme => ({ link: theme.link }));

const AuthMenu = props => {
    const { t } = useTranslation();
    const { anchorEl, handleClose, signout } = props;

    const classes = useStyles();

    const handleSignOut = () => {
        handleClose();
        history.push('/');
        signout();
    };

    const locale = t('authMenu', { returnObjects: true });

    return (
        <Menu
            id="customized-menu"
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center'
            }}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}>
            <Link to="/cabinet" className={classes.link}>
                <MenuItem>
                    <ListItemIcon>
                        <AccountBox fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary={locale.cabinet} />
                </MenuItem>
            </Link>
            <MenuItem onClick={handleSignOut}>
                <ListItemIcon>
                    <ExitToApp fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={locale.signout} />
            </MenuItem>
        </Menu>
    );
};

export default connect(null, { signout })(AuthMenu);
