import React from 'react';
import { Menu, MenuItem, ListItemText } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const LangMenu = props => {
    const { i18n } = useTranslation();
    const { anchorEl, handleClose } = props;

    const chooseLanguage = lang => {
        i18n.changeLanguage(lang);
        localStorage.setItem('lang', lang);
        handleClose();
    };

    return (
        <Menu
            id="customized-menu"
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center'
            }}
            transformOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
            }}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}>
            <MenuItem onClick={() => chooseLanguage('en')}>
                <ListItemText primary="English" />
            </MenuItem>
            <MenuItem onClick={() => chooseLanguage('ru')}>
                <ListItemText primary="Русский" />
            </MenuItem>
        </Menu>
    );
};

export default LangMenu;
