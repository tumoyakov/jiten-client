import React, { useState, useEffect } from 'react';
import {
  Button,
  TextField,
  ListItem,
  ListItemText,
  List,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import {
  addKanjiList,
  addWordList,
  getKanjiLists,
  getWordLists,
  addKanjiToList,
  addWordToList,
} from '../../actions';
import Loading from './Loading';
import { connect } from 'react-redux';
import AddListDialog from './dialogs/AddListDialog';

const AddKanjiToListDialog = (props) => {
  const {
    itemId,
    type,
    open,
    handleClose,
    kanjiLists,
    wordLists,
    addKanjiList,
    addWordList,
    getKanjiLists,
    getWordLists,
    addKanjiToList,
    addWordToList,
  } = props;
  const { t } = useTranslation();
  const locale = t('AddToListDialog', { returnObjects: true });

  const [loading, setLoading] = useState(false);

  const [openAddDialog, setAddDialogOpen] = useState(false);

  const handleAddDialogClose = () => {
    setAddDialogOpen(false);
  };

  const radioGroupRef = React.useRef(null);

  useEffect(() => {
    setLoading(true);
    if (type === 'kanjis') {
      getKanjiLists()
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    } else {
      getWordLists()
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    }
  }, []);

  const [error, setError] = useState('');

  const handleEntering = () => {
    if (radioGroupRef.current != null) {
      radioGroupRef.current.focus();
    }
  };

  const renderListItem = (
    id,
    name,
    index //TODO fix duplicated key
  ) => (
    <ListItem
      key={`${id}-${type}-${itemId}`}
      button
      divider
      role="listitem"
      onClick={() => handleChoose(id, index)}>
      <ListItemText primary={name} />
    </ListItem>
  );

  const handleChoose = (listId, index) => {
    console.log(itemId, listId);
    if (type === 'kanji') {
      addKanjiToList(itemId, listId, index)
        .then(() => {
          handleClose();
        })
        .catch((err) => {
          setError(err.toString());
        });
    } else {
      addWordToList(itemId, listId, index)
        .then(() => {
          handleClose();
        })
        .catch((err) => {
          console.log(locale[err], err);
          setError(locale[err]);
        });
    }
  };

  function renderList() {
    return (
      <List component="div" role="list">
        {type === 'kanji'
          ? kanjiLists.map((item, index) =>
              renderListItem(item.list.id, item.list.name, index)
            )
          : wordLists.map((item, index) =>
              renderListItem(item.list.id, item.list.name, index)
            )}
      </List>
    );
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      onEntering={handleEntering}
      aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{locale.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{locale.describe}</DialogContentText>
        {loading ? <Loading /> : renderList()}
        <AddListDialog
          open={openAddDialog}
          handleClose={handleAddDialogClose}
          handleListCreate={type === 'kanji' ? addKanjiList : addWordList}
        />
        {error ? (
          <Typography color="error" variant="caption">
            {error}
          </Typography>
        ) : null}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setAddDialogOpen(true);
          }}
          color="primary">
          {locale.addList}
        </Button>
        <Button
          onClick={() => {
            handleClose();
          }}
          color="primary">
          {locale.cancel}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const mapStateToProps = (state) => {
  return {
    kanjiLists: Object.values(state.kanjiLists.lists),
    wordLists: Object.values(state.wordLists.lists),
  };
};

export default connect(mapStateToProps, {
  addKanjiList,
  addWordList,
  getKanjiLists,
  getWordLists,
  addKanjiToList,
  addWordToList,
})(AddKanjiToListDialog);
