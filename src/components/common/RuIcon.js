import React from "react";
import Icon from "../../static/lang/ru.svg";
import { SvgIcon } from "@material-ui/core";

const RuIcon = () => {
  return (
    <SvgIcon>
      <Icon />
    </SvgIcon>
  );
};

export default RuIcon;
