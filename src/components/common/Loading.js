import React from 'react';
import { Typography, CircularProgress, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    loading: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        height: '100%',
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    progress: {
        marginRight: theme.spacing(1),
    },
}));

const Loading = () => {
    const classes = useStyles();
    return (
        <div className={classes.loading}>
            <CircularProgress className={classes.progress} />
            <Typography variant="subtitle1">Загрузка</Typography>
        </div>
    );
};

export default Loading;
