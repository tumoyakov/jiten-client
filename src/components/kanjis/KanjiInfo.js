import React from 'react';
import {
    Chip,
    Divider,
    Typography,
    makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(1),
        width: '100%'
    },
    item: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    chip: {
        width: '100%'
    },
    itemText: {
        fontWeight: '300'
    },
    sectionTitle: {
        fontWeight: '400',
        marginTop: theme.spacing(1),
    }
}));

const KanjiInfo = props => {
    const classes = useStyles();
    const { jlpt, strokeCount, radical, grade, freq, locale } = props;

    const renderRadical = () => {
        const rad = radical.radical;
        const meaning = radical.meaning;
        return (
            <div className={classes.item}>
                <Typography variant="body1" className={classes.itemText}>
                    {rad}
                </Typography>
                <Typography variant="body1" className={classes.itemText}>
                    {meaning}
                </Typography>
            </div>
        )
    }

    return (
       <div className={classes.root}>
           { 
                jlpt ? (
                    <div className={classes.item}>
                            <Chip 
                                className={classes.chip}
                                variant="outlined"
                                size="small"
                                color="primary"
                                label={`${locale.jlpt}${jlpt}`}
                            />
                    </div>
                ) : null 
            }
            { 
                strokeCount ? (
                    <div className={classes.item}>
                            <Chip 
                                className={classes.chip}
                                variant="outlined"
                                size="small"
                                color="primary"
                                label={`${locale.strokeCount}${strokeCount}`}
                            />
                    </div>
                ) : null 
            }
            { 
                grade ? (
                    <div className={classes.item}>
                            <Chip 
                                className={classes.chip}
                                variant="outlined"
                                size="small"
                                color="primary"
                                label={`${locale.grade}${grade}`}
                            />
                    </div>
                ) : null 
            }
            { 
                freq ? (
                    <div className={classes.item}>
                        <Typography variant="body2" className={classes.sectionTitle}>
                            {`${freq}${locale.freq}`}
                        </Typography>
                    </div>
                ) : null 
            }
            {
                radical ? (
                    <>
                        <Typography variant="subtitle1" className={classes.sectionTitle}>
                            {locale.radical} {`#${radical.num}`}
                            <Divider />
                        </Typography>
                        {renderRadical()}
                    </>
                ) : null
            }
            
       </div>
    );
};

export default KanjiInfo;
