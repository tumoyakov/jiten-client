import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import clsx from 'clsx';
import { Typography, makeStyles } from '@material-ui/core';
import { fetchKanji } from '../../actions';
import KanjiInfo from './KanjiInfo';
import KanjiData from './KanjiData';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'row',
        [theme.downMediaQuery]: {
            flexDirection: 'column',
        },
    },
    leftSide: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    rightSide: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(1),
    },
    menu: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    menuButton: {
        textDecoration: 'none',
    },
    literal: {
        fontWeight: '300',
    },
    literalWrapper: {
        width: '200px',
        height: '200px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

const KanjiDetail = (props) => {
    const { id } = props.match.params;
    const classes = useStyles();

    const { t, i18n } = useTranslation();

    useEffect(() => {
        props.fetchKanji(id);
    }, []);

    const { data } = props; // TODO fix this

    if (!data) {
        return <div>Loading...</div>; //todo create loading component
    }

    const langMeanings =
        Boolean(data.kanjiMeanings) && i18n.language !== 'en'
            ? data.kanjiMeanings.filter((item) => {
                  return item.lang === 'ru' || item.lang === 'rus';
              })
            : null;

    const meanings =
        Boolean(langMeanings) && langMeanings.length !== 0
            ? langMeanings
            : data.kanjiMeanings
            ? data.kanjiMeanings.filter((item) => {
                  return item.lang === 'en' || item.lang === 'eng';
              })
            : [];

    const readings = data.kanjiReadings ? data.kanjiReadings : [];
    const nanories = data.nanories ? data.nanories : [];

    const locale = t('kanjiDetail', { returnObjects: true });

    const kanjiSVG = require(`../../static/kanji/0${data.codepoint}.svg`);

    return (
        <div className={classes.root}>
            <div className={classes.cardContent}>
                <div className={classes.leftSide}>
                    {kanjiSVG ? (
                        <img src={kanjiSVG} width="200px" alt="kanji svg" />
                    ) : (
                        <div className={classes.literalWrapper}>
                            <Typography
                                variant="h1"
                                align="center"
                                className={classes.literal}>
                                {data.literal}
                            </Typography>
                        </div>
                    )}

                    <KanjiInfo
                        jlpt={data.jlpt}
                        strokeCount={data.strokeCount}
                        radical={data.Radical}
                        grade={data.grade}
                        freq={data.freq}
                        locale={locale}
                    />
                </div>
                <div className={classes.rightSide}>
                    <KanjiData
                        meanings={meanings}
                        readings={readings}
                        nanories={nanories}
                        locale={locale}
                    />
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state, ownProps) => {
    return {
        data: state.kanjis[ownProps.match.params.id],
    };
};

export default connect(mapStateToProps, { fetchKanji })(KanjiDetail);
