import React from 'react';
import { Typography, Link, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  subtitle: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const About = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography className={classes.title} variant="h4">
        О разработчике
      </Typography>
      <Typography variant="body1">
        Данный проект разрабатывался в качестве магистерской работы студентом
        группы М-21 ХГУ им. Н.Ф. Катанова Тумояковым Л. А.
      </Typography>
      <Typography variant="body1">
        Научный руководитель доцент, д-р. техн. наук Дулесов А. С
      </Typography>
      <Typography variant="h6">Контактные данные:</Typography>
      <Typography variant="body1">
        e-mail: leonid.tumoyakov@gmail.com, twitter: @tumoyakov
      </Typography>
      <Typography className={classes.title} variant="h4">
        Используемые источники
      </Typography>
      <Typography variant="body1">
        Данные используемые в данном проекте были взяты из различных открытых
        источников. Я выражаю глубокую признательность людям, работающим над
        данными проектами, благодаря им возможны подобные проекты.
      </Typography>
      <Link href="http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project">
        <Typography variant="h6">JMDict</Typography>
      </Link>
      <Typography variant="body1">
        JMdict, created by Jim Breen and now managed by the Electronic
        Dictionary Research and Development Group (EDRDG), is a great general
        dictionary with roughly 170 000 entries and is actively maintained by
        Jim Breen and a team of volunteers. This dictionary file is the source
        of the bulk of the words in Jisho.
      </Typography>
      <Link href="http://www.edrdg.org/wiki/index.php/KANJIDIC_Project">
        <Typography variant="h6">KANJIDIC2</Typography>
      </Link>
      <Typography variant="body1">
        KANJIDIC2, also from Jim Breen/EDRDG, is a database of kanji that
        includes readings, meanings and a lot of metadata around kanji like
        lookup numbers for kanji dictionary books, stroke count and information
        about variant forms.
      </Typography>
      <Link href="http://www.edrdg.org/krad/kradinf.html">
        <Typography variant="h6">RADKFILE</Typography>
      </Link>
      <Typography variant="body1">
        RADKFILE, also from Jim Breen/EDRDG, is a database of the radicals that
        make up kanji. This is used to drive the radical lookup feature in
        Jisho.
      </Typography>
      <Link href="http://kanjivg.tagaini.net/">
        <Typography variant="h6">KanjiVG</Typography>
      </Link>
      <Typography variant="body1">
        Stroke order data for kanji come from the excellent KanjiVG project, by
        Ulrich Apel and several contributors.
      </Typography>
    </div>
  );
};

export default About;
