import React, { useState } from 'react';
import {
    SwipeableDrawer,
    Button,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    IconButton,
    Divider,
    makeStyles,
    useTheme,
} from '@material-ui/core';
import {
    AccountCircle,
    ViewList,
    Search,
    ChevronRight,
    ChevronLeft,
    Equalizer,
    Settings,
} from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { noop } from '../const';

const useStyles = makeStyles((theme) => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    link: {
        color: theme.palette.text.primary,
        textDecoration: 'none',
    },
}));

const MobileDrawer = (props) => {
    const classes = useStyles();
    const theme = useTheme();

    const { open, handleDrawerClose } = props;
    const { t } = useTranslation();
    const locale = t('MobileDrawer', { returnObjects: true });

    const list = () => (
        <div className={classes.list} role="presentation">
            <div className={classes.drawerHeader}>
                <IconButton onClick={handleDrawerClose}>
                    {theme.direction === 'ltr' ? (
                        <ChevronLeft />
                    ) : (
                        <ChevronRight />
                    )}
                </IconButton>
            </div>
            <Divider />
            <List onClick={handleDrawerClose}>
                <Link to="/" className={classes.link}>
                    <ListItem button>
                        <ListItemIcon>
                            <Search />
                        </ListItemIcon>
                        <ListItemText primary={locale.search} />
                    </ListItem>
                </Link>
                <Link to="/lists" className={classes.link}>
                    <ListItem button>
                        <ListItemIcon>
                            <ViewList />
                        </ListItemIcon>
                        <ListItemText primary={locale.kanjiList} />
                    </ListItem>
                </Link>
                <Link to="/lists/words" className={classes.link}>
                    <ListItem button>
                        <ListItemIcon>
                            <ViewList />
                        </ListItemIcon>
                        <ListItemText primary={locale.wordList} />
                    </ListItem>
                </Link>
                <Link to="/cabinet" className={classes.link}>
                    <ListItem button>
                        <ListItemIcon>
                            <Equalizer />
                        </ListItemIcon>
                        <ListItemText primary={locale.statistics} />
                    </ListItem>
                </Link>
                <Link to="/cabinet/settings" className={classes.link}>
                    <ListItem button>
                        <ListItemIcon>
                            <Settings />
                        </ListItemIcon>
                        <ListItemText primary={locale.settings} />
                    </ListItem>
                </Link>
            </List>
        </div>
    );

    return (
        <div>
            <React.Fragment>
                <SwipeableDrawer
                    anchor="left"
                    open={open}
                    onClose={noop}
                    onOpen={noop}>
                    {list()}
                </SwipeableDrawer>
            </React.Fragment>
        </div>
    );
};

export default MobileDrawer;
