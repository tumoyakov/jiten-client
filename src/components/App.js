import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { connect } from 'react-redux';
import history from '../history';
import { Container, ThemeProvider, makeStyles } from '@material-ui/core';
import '../localization/i18n';
import { useTranslation } from 'react-i18next';

import { refreshUser } from '../actions';
import { AppTheme } from '../utils/AppTheme';
import AppSwitch from './switch/AppSwitch';
import Header from './Header';
import Footer from './Footer';
import { useMobileView } from '../decorators';
import MobileDrawer from './MobileDrawer';
import Loading from './common/Loading';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    'flex-direction': 'column',
  },
  content: {
    width: '100%',
    height: '100%',
    'overflow-y': 'auto',
  },
  container: {
    width: '100%',
    minHeight: '100%',
  },
}));

const App = (props) => {
  const classes = useStyles();
  const isMobile = useMobileView();

  const [isMobileDrawerOpen, setMobileDrawerOpen] = useState(false);

  const language = localStorage.getItem('lang');
  const { i18n } = useTranslation();
  if (language && i18n.language !== language) {
    i18n.changeLanguage(language);
  }

  const token = localStorage.getItem('token');

  if (!props.session.auth && token) {
    props.refreshUser();
    return (
      <div className={classes.root}>
        <Loading />
      </div>
    );
  }

  const handleCloseMobileDrawer = () => {
    setMobileDrawerOpen(false);
  };

  const handleMobileDrawer = () => {
    console.log('handleMobileDrawer');
    setMobileDrawerOpen(!isMobileDrawerOpen);
  };

  const renderMobileDrawer = () =>
    isMobile ? (
      <MobileDrawer
        open={isMobileDrawerOpen}
        handleDrawerClose={handleCloseMobileDrawer}
      />
    ) : null;

  const renderContent = () =>
    isMobile ? (
      <AppSwitch />
    ) : (
      <Container maxWidth="md" className={classes.container}>
        <AppSwitch />
      </Container>
    );

  return (
    <div className={classes.root}>
      <ThemeProvider theme={AppTheme}>
        <Router history={history}>
          <Header handleMobileDrawer={handleMobileDrawer} />
          {renderMobileDrawer()}
          <div className={classes.content}>{renderContent()}</div>
          {!isMobile ? <Footer /> : null}
        </Router>
      </ThemeProvider>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    session: state.session,
  };
};

export default connect(mapStateToProps, { refreshUser })(App);
