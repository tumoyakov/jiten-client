import React, { useState } from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  IconButton,
  Button,
} from '@material-ui/core';
import { AccountCircle, MenuBook, ViewList, Menu } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { useMobileView } from '../decorators';
import AuthMenu from './common/menu/AuthMenu';
import { noop } from '../const';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  titleLink: {
    color: '#fff',
    textDecoration: 'none',
  },
  icons: {
    color: '#fff',
  },
  menuButton: {
    height: '100%',
    color: '#fff',
  },
}));

const Header = (props) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const { handleMobileDrawer } = props;

  const isMobileView = useMobileView();
  const isAuthenticated = props.session.auth;

  const locale = t('header', { returnObjects: true });
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const renderListButton = (isMobile) => {
    return isMobile ? null : (
      <Link to="/lists" className={classes.titleLink}>
        <Button
          startIcon={<ViewList />}
          className={classes.menuButton}
          onClick={noop}>
          {locale.list}
        </Button>
      </Link>
    );
  };

  const renderUserButton = (isMobile) => {
    return isMobile ? (
      <IconButton
        color="inherit"
        onClick={isAuthenticated ? handleClick : noop}>
        <AccountCircle className={classes.icons} />
      </IconButton>
    ) : (
      <Button
        startIcon={<AccountCircle />}
        className={classes.menuButton}
        onClick={isAuthenticated ? handleClick : noop}>
        {isAuthenticated ? locale.menu : locale.signin}
      </Button>
    );
  };

  const renderReadingButton = (isMobile) => {
    return isMobile ? null : (
      <Link to="/reading" className={classes.titleLink}>
        <Button
          startIcon={<MenuBook />}
          className={classes.menuButton}
          onClick={noop}>
          {locale.reading}
        </Button>
      </Link>
    );
  };

  return (
    <AppBar position="static">
      <Toolbar>
        {isMobileView ? (
          <IconButton color="inherit" onClick={handleMobileDrawer}>
            <Menu className={classes.icons} />
          </IconButton>
        ) : null}
        <Typography variant="h6" className={classes.title}>
          <Link to="/" className={classes.titleLink}>
            Jiten
          </Link>
        </Typography>
        {renderReadingButton()}
        {isAuthenticated ? renderListButton(isMobileView) : null}
        {!isAuthenticated ? (
          <Link to="/login" className={classes.titleLink}>
            {renderUserButton(isMobileView)}
          </Link>
        ) : (
          renderUserButton(isMobileView)
        )}
        <AuthMenu anchorEl={anchorEl} handleClose={handleClose} />
      </Toolbar>
    </AppBar>
  );
};

const mapStateToProps = (state) => {
  return {
    session: state.session,
  };
};

export default connect(mapStateToProps, {})(Header);
