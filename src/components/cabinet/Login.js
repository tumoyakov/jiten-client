import React, { useState } from 'react';
import { Paper, Button, Typography, makeStyles } from '@material-ui/core';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { signin, refreshUser } from '../../actions';
import { checkEmailValidation } from '../../utils';
import TextFieldForm from '../common/form/TextFieldForm';
import PasswordFieldForm from '../common/form/PasswordFieldForm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    'flex-direction': 'row',
    'align-items': 'center',
    'justify-content': 'center',
  },
  auth: {
    width: '50%',
    padding: theme.spacing(2),
    margin: theme.spacing(2),
    [theme.downMediaQuery]: {
      width: '100%',
      padding: theme.spacing(2),
      margin: '0',
    },
  },
  title: {
    color: theme.palette.primary.main,
  },
  authForm: {
    width: '100%',
  },
  formInput: {
    width: '100%',
    marginBottom: theme.spacing(1),
  },
  formElement: {
    width: '100%',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  registerLink: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
  },
}));

const Login = (props) => {
  const classes = useStyles();

  const { t } = useTranslation();
  const [submitError, setSubmitError] = useState(null);

  const locale = t('login', { returnObjects: true });

  const submit = ({ email, password }) => {
    setSubmitError(null);

    if (!email) {
      throw new SubmissionError({
        email: locale.email,
        _error: 'Email failed!',
      });
    } else if (!checkEmailValidation(email)) {
      throw new SubmissionError({
        email: locale.emailNotCorrect,
        _error: 'Email failed!',
      });
    }

    if (!password || password.length < 5) {
      throw new SubmissionError({
        password: locale.passwordNotCorrect,
        _error: 'Password failed!',
      });
    }

    props
      .signin({ email, password })
      .then((result) => {
        props.history.push('/');
      })
      .catch((error) => {
        setSubmitError(error);
      });
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.auth}>
        <Typography className={classes.title} variant="h4" component="h2">
          {locale.title}
        </Typography>
        <form
          onSubmit={props.handleSubmit(submit.bind(this))}
          onChange={() => setSubmitError(null)}
          className={classes.authForm}>
          <Field
            name="email"
            component={TextFieldForm}
            label={locale.email}
            type="email"
          />
          <Field
            name="password"
            component={PasswordFieldForm}
            label={locale.password}
          />
          <Button
            type="submit"
            className={classes.formElement}
            variant="outlined"
            error={Boolean(submitError)}
            color="primary">
            {locale.button}
          </Button>
        </form>
        {submitError ? (
          <Typography variant="body2" color="error">
            {locale[submitError]}
          </Typography>
        ) : null}
        <Link to="/registration" className={classes.registerLink}>
          <Typography variant="subtitle1" component="h2">
            {locale.toRegistration}
          </Typography>
        </Link>
      </Paper>
    </div>
  );
};

const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Введите email';
  } else if (!checkEmailValidation(values.email)) {
    errors.email = 'Некорректный email';
  }

  if (!values.password) {
    errors.password = 'Введите пароль';
  }

  return errors;
};

export default connect(null, { signin, refreshUser })(
  reduxForm({
    form: 'login',
    validate,
  })(Login)
);
