import React, {useState} from 'react';
import {
    Paper,
    Button,
    Typography,
    TextField,
    makeStyles
} from '@material-ui/core';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { signup } from '../../actions';
import { checkEmailValidation } from '../../utils';
import TextFieldForm from '../common/form/TextFieldForm';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        display: 'flex',
        'flex-direction': 'row',
        'align-items': 'center',
        'justify-content': 'center',
        [theme.downMediaQuery]: {
            height: 'auto'
        }
    },
    auth: {
        width: '50%',
        padding: theme.spacing(2),
        margin: theme.spacing(2),
        [theme.downMediaQuery]: {
            width: '100%',
            padding: theme.spacing(2),
            margin: '0',
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2)
        }
    },
    title: {
        color: theme.palette.primary.main
    },
    authForm: {
        width: '100%',
        display: 'flex',
        'flex-direction': 'column'
    },
    formInput: {
        width: '100%',
        marginBottom: theme.spacing(1)
    },
    formElement: {
        width: '100%',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    registerLink: {
        color: theme.palette.primary.main,
        textDecoration: 'none'
    }
}));

const Registration = props => {
    const classes = useStyles();
    const { t } = useTranslation();
    const [submitError, setSubmitError] = useState(null);

    const locale = t('registration', { returnObjects: true });

    const submit = ({ username, email, password }) => {
        setSubmitError(null);
        if (!email) {
            throw new SubmissionError({
                email: locale.email,
                _error: 'Email failed!'
            });
        } else if (!checkEmailValidation(email)) {
            throw new SubmissionError({
                email: locale.emailIsNotCorrect,
                _error: 'Email failed!'
            });
        }

        if (!password || password.length < 5) {
            throw new SubmissionError({
                password: locale.passwordIsNotCorrect,
                _error: 'Password failed!'
            });
        }

        props.signup({ login: username, email, password }).then(result => {
            if (result && result.error) {
                setSubmitError(t('serverError'));
            }
            else props.history.push('/login');
        });
    };

    return (
        <div className={classes.root}>
            <Paper className={classes.auth}>
                <Typography
                    className={classes.title}
                    variant="h4"
                    component="h2">
                    {locale.title}
                </Typography>
                <form
                    onSubmit={props.handleSubmit(submit.bind(this))}
                    onChange={() => setSubmitError(null)}
                    className={classes.authForm}>
                    <Field
                        name="email"
                        component={TextFieldForm}
                        label={locale.email}
                        type="email"
                    />
                    <Field
                        name="username"
                        component={TextFieldForm}
                        label={locale.login}
                    />
                    <Field
                        name="password"
                        component={TextFieldForm}
                        label={locale.password}
                        type="password"
                    />
                    <Field
                        name="repassword"
                        component={TextFieldForm}
                        label={locale.repassword}
                        type="password"
                    />
                    <Button
                        type="submit"
                        className={classes.formElement}
                        variant="outlined"
                        color="primary">
                        {locale.button}
                    </Button>
                </form>
                <Link to="/login" className={classes.registerLink}>
                    <Typography variant="subtitle1" component="h2">
                        {locale.toLogin}
                    </Typography>
                </Link>
            </Paper>
        </div>
    );
};

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Введите email';
    } else if (!checkEmailValidation(values.email)) {
        errors.email = 'Некорректный email';
    }

    if (!values.password) {
        errors.password = 'Введите пароль';
    }

    if (!values.repassword) {
        errors.repassword = 'Подтвердите пароль';
    } else if (
        values.repassword.length > 0 &&
        values.repassword !== values.password
    ) {
        errors.repassword = 'Пароли не совпадают';
    }

    if (!values.username) {
        errors.username = 'Введите имя пользователя';
    }

    return errors;
};

export default connect(null, { signup })(
    reduxForm({
        form: 'registration',
        validate
    })(Registration)
);
