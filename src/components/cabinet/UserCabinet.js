import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import LeftMenu from './LeftMenu';
import Settings from './Settings';
import Statistics from './Statistics';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        display: 'flex',
        'flex-direction': 'row',
        padding: theme.spacing(1),
    },
    content: {
        flex: '3 1 0',
        width: '100%',
        minHeight: '100%',
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
}));

const UserCabinet = (props) => {
    const classes = useStyles();

    let { path, url } = useRouteMatch();

    return (
        <div className={classes.root}>
            <LeftMenu />
            <div className={classes.content}>
                <Switch>
                    <Route path={`${path}`} exact component={Statistics} />
                    <Route
                        path={`${path}/settings`}
                        exact
                        component={Settings}
                    />
                </Switch>
            </div>
        </div>
    );
};

export default UserCabinet;
