import React from 'react';
import { makeStyles, Typography, Paper } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    title: {
        'font-weight': '300',
        marginBottom: theme.spacing(1),
    },
    formPaper: {
        padding: '16px',
        marginBottom: theme.spacing(1),
    },
    paperItem: {
        marginBottom: theme.spacing(1),
    },
}));

const Statistics = (props) => {
    const classes = useStyles();
    const { t } = useTranslation();

    const locale = t('cabinetLeftMenu', { returnObjects: true });
    return (
        <div className={classes.root}>
            <Typography variant="h3" className={classes.title}>
                Статистика
            </Typography>
            <Typography variant="h6" className={classes.title}>
                Списки
            </Typography>
            <Paper variant="outlined" className={classes.formPaper}>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Всего создано списков: 8
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Списков кандзи: 8
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Списков слов: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Всего кандзи в списках: 10
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Всего слов в списках: 0
                </Typography>
            </Paper>
            <Typography variant="h6" className={classes.title}>
                Иероглифы
            </Typography>
            <Paper variant="outlined" className={classes.formPaper}>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Всего изучается иероглифов: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 5: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 4: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 3: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 2: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 1: 0
                </Typography>
            </Paper>
            <Typography variant="h6" className={classes.title}>
                Слова
            </Typography>
            <Paper variant="outlined" className={classes.formPaper}>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    Всего изучается слов: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 5: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 4: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 3: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 2: 0
                </Typography>
                <Typography
                    variant="body1"
                    className={classes.paperItem}
                    component="div">
                    JLPT 1: 0
                </Typography>
            </Paper>
        </div>
    );
};

export default Statistics;
