import React, { useState } from 'react';
import {
    makeStyles,
    Typography,
    Button,
    Paper
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PasswordFieldForm from '../common/form/PasswordFieldForm';
import { changePassword } from '../../actions'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    title: {
        'font-weight': '300',
        marginBottom: theme.spacing(1)
    },
    formPaper: {
        padding: '16px'
    },
    authForm: {
        width: '100%'
    },
    formInput: {
        width: '100%',
        marginBottom: theme.spacing(1)
    },
    formElement: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
}));

const Settings = (props) => {
    const classes = useStyles();
    const { t } = useTranslation();
    const [submitError, setSubmitError] = useState(null);
    
    const locale = t('cabinetSettings',  { returnObjects: true });

    function changePassword({oldPassword, newPassword}) {
        if (!oldPassword) {
            throw new SubmissionError({
                oldPassword: locale.passwordError,
                _error: 'Old Password failed!'
            });
        }
        if (!newPassword) {
            throw new SubmissionError({
                newPassword: locale.passwordError,
                _error: 'New Password failed!'
            });
        }

        props.changePassword({ oldPassword, newPassword }).then(result => {
            if (result && result.error) {
                setSubmitError(t('serverError'));
            }
            else props.history.push('/');
        });
    }

    return (
        <div className={classes.root}>
            <Typography variant="h3" className={classes.title}>{locale.title}</Typography>
            <Typography variant="h6" className={classes.title}>{locale.changePasswordTitle}</Typography>
            <Paper variant="outlined" className={classes.formPaper}>
                <form
                    onSubmit={props.handleSubmit(changePassword.bind(this))}
                    onChange={() => setSubmitError(null)}
                    className={classes.authForm}>
                    <Field
                        name="oldPassword"
                        component={PasswordFieldForm}
                        label={locale.oldPassword}
                    />
                    <Field
                        name="newPassword"
                        component={PasswordFieldForm}
                        label={locale.newPassword}
                    />
                    <Field
                        name="reNewPassword"
                        component={PasswordFieldForm}
                        label={locale.reNewPassword}
                    />
                    <Button
                        type="submit"
                        className={classes.formElement}
                        variant="outlined"
                        error={Boolean(submitError)}
                        color="primary">
                        {locale.button}
                    </Button>
                </form>
            </Paper>
        </div>
    );
}

export default reduxForm({
    form: 'settings'
})(Settings);