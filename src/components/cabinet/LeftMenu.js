import React from 'react';
import { Link } from 'react-router-dom';
import { Paper, MenuList, MenuItem, makeStyles } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useMobileView } from '../../decorators';

const useStyles = makeStyles((theme) => ({
    root: {
        flex: '1 1 0',
        width: '100%',
        marginTop: theme.spacing(1),
    },
    paper: {
        marginRight: theme.spacing(2),
    },
    link: {
        color: theme.palette.text.primary,
        textDecoration: 'none',
    },
}));

const LeftMenu = (props) => {
    const classes = useStyles();
    const { t } = useTranslation();
    const isMobile = useMobileView();

    const locale = t('cabinetLeftMenu', { returnObjects: true });
    return !isMobile ? (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <MenuList>
                    <Link to="/cabinet/" className={classes.link}>
                        <MenuItem>{locale.statistics}</MenuItem>
                    </Link>
                    <Link to="/cabinet/settings" className={classes.link}>
                        <MenuItem>{locale.settings}</MenuItem>
                    </Link>
                </MenuList>
            </Paper>
        </div>
    ) : null;
};

export default LeftMenu;
