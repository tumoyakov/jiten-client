import _ from 'lodash';
import { FETCH_KANJIPARTS } from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_KANJIPARTS: {
            return {
                ...state,
                parts: { ..._.mapKeys(action.payload, 'id') }
            };
        }
        default:
            return state;
    }
};
