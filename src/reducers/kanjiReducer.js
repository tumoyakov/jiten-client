import _ from 'lodash';
import {
  FETCH_KANJI,
  GET_KANJI_LIST,
  SEARCH,
  FETCH_KANJIS,
} from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case SEARCH:
      let kanjiIds = new Set();
      let kanjis = [];
      for (let result of action.payload.results) {
        for (let kanji of result.kanjis) {
          if (!kanjiIds.has(kanji.id)) {
            kanjis.push(kanji);
            kanjiIds.add(kanji.id);
          }
        }
      }
      return { ...state, ..._.mapKeys(kanjis, 'id') };
    case GET_KANJI_LIST:
      return { ...state, ..._.mapKeys(action.payload.kanjis, 'id') };
    case FETCH_KANJI:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case FETCH_KANJIS:
      return { ...state, ..._.mapKeys(action.payload.kanjis, 'id') };
    default:
      return state;
  }
};
