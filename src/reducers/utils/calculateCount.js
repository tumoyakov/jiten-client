export default function calculateCount(list, elemPerPage = 10) {
    return list && list.length <= elemPerPage
        ? 0
        : list.length % elemPerPage !== 0
        ? parseInt(list.length / elemPerPage) + 1
        : parseInt(list.length / elemPerPage);
}
