export default function calculateCountByLength(length, elemPerPage = 10) {
    return length <= elemPerPage
        ? 0
        : length % elemPerPage !== 0
        ? parseInt(length / elemPerPage) + 1
        : parseInt(length / elemPerPage);
}
