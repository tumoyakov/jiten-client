import _ from 'lodash';
import { SET_LIST_TO_TEST } from '../actions/types';

const initialState = {
    list: null,
    type: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_LIST_TO_TEST:
            return {
                ...state,
                list: action.payload.list,
                type: action.payload.type,
            };
        default:
            return state;
    }
};
