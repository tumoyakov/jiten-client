import _ from 'lodash';
import {
  GET_WORD_LISTS,
  ADD_WORD_LIST,
  GET_WORD_LIST,
  UPDATE_WORD_LIST,
  DELETE_WORD_LIST,
  DELETE_WORD_FROM_LIST,
  ADD_WORD_TO_LIST,
  CHANGE_PAGE_WORD_LIST,
  CHANGE_PAGE_IN_WORD_LIST,
} from '../actions/types';
import { calculateCount } from './utils';

const initialState = {
  lists: {},
  count: 0,
  page: 1,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_WORD_LISTS: {
      const lists = action.payload;
      console.log(lists);
      const wordListsCount = calculateCount(lists);
      const newState = {
        lists: {
          ..._.mapKeys(
            lists.map((item) => ({ ...item, page: 1 })),
            'list.id'
          ),
        },
        count: wordListsCount,
        page: 1,
      };
      console.log(newState);
      return {
        ...state,
        ...newState,
      };
    }
    case ADD_WORD_LIST: {
      const data = action.payload;
      const lists = {
        ...state.lists,
        ..._.mapKeys([{ ...data, page: 1 }], 'list.id'),
      };
      const wordListsCount = calculateCount(state.lists);
      return { ...state, lists, count: wordListsCount };
    }
    case GET_WORD_LIST: {
      const { list, words } = action.payload;
      const lists = state.lists;
      lists[list.id] = {
        list,
        words: words.map((item) => item.id),
        page: 1,
        count: words && words.length,
      };
      return { ...state, lists, count: calculateCount(lists) };
    }
    case UPDATE_WORD_LIST: {
      const { data } = action.payload;
      const lists = { ..._.omit(state.lists, `${data.list.id}`), data };
      return { ...state, lists };
    }
    case DELETE_WORD_LIST: {
      const { id } = action.payload;
      const lists = _.omit(state.lists, `${id}`);
      return { ...state, lists };
    }
    case ADD_WORD_TO_LIST: {
      const { data, listIndex } = action.payload;
      const lists = state.lists;
      lists[listIndex] = { ...data, page: 1 };
      return { ...state, lists };
    }
    case DELETE_WORD_FROM_LIST: {
      const { wordId, listId } = action.payload;
      console.log(listId);
      state.lists[listId].words = state.lists[listId].words.filter(
        (item) => item !== wordId
      );
      state.lists[listId].page = 1;
      return { ...state };
    }
    case CHANGE_PAGE_WORD_LIST: {
      return {
        ...state,
        page: action.payload.page,
      };
    }
    case CHANGE_PAGE_IN_WORD_LIST: {
      const { listId, page } = action.payload;
      console.log(listId, page);
      state.lists[listId] = { ...state.lists[listId], page };
      return { ...state };
    }
    default:
      return state;
  }
};
