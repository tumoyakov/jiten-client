import _ from 'lodash';
import {
  GET_KANJI_LIST,
  GET_KANJI_LISTS,
  ADD_KANJI_LIST,
  UPDATE_KANJI_LIST,
  DELETE_KANJI_LIST,
  ADD_KANJI_TO_LIST,
  DELETE_KANJI_FROM_LIST,
  CHANGE_PAGE_KANJI_LIST,
  CHANGE_PAGE_IN_KANJI_LIST,
} from '../actions/types';
import { calculateCount } from './utils';

const initialState = {
  lists: {},
  count: 0,
  page: 1,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_KANJI_LISTS: {
      const { lists } = action.payload;
      const kanjiListsCount = calculateCount(lists);
      const newState = {
        lists: {
          ..._.mapKeys(
            lists.map((item) => ({ ...item, page: 1 })),
            'list.id'
          ),
        },
        count: kanjiListsCount,
        page: 1,
      };
      return {
        ...state,
        ...newState,
      };
    }
    case ADD_KANJI_LIST: {
      const data = action.payload;
      const lists = {
        ...state.lists,
        ..._.mapKeys([{ ...data, page: 1 }], 'list.id'),
      };
      const kanjiListsCount = calculateCount(state.lists);
      return { ...state, lists, count: kanjiListsCount };
    }
    case GET_KANJI_LIST: {
      const { list, kanjis } = action.payload;
      const lists = state.lists;
      lists[list.id] = {
        list,
        kanjis: kanjis.map((item) => item.id),
        page: 1,
        count: kanjis && kanjis.length,
      };
      return { ...state, lists, count: calculateCount(lists) };
    }
    case UPDATE_KANJI_LIST: {
      const { data } = action.payload;
      const lists = { ..._.omit(state.lists, `${data.list.id}`), data };
      return { ...state, lists };
    }
    case DELETE_KANJI_LIST: {
      const { id } = action.payload;
      const lists = _.omit(state.lists, `${id}`);
      return { ...state, lists };
    }
    case ADD_KANJI_TO_LIST: {
      const { data, listIndex } = action.payload;
      const lists = state.lists;
      lists[listIndex] = { ...data, page: 1 };
      return { ...state, lists };
    }
    case DELETE_KANJI_FROM_LIST: {
      const { kanjiId, listId } = action.payload;
      console.log(listId);
      state.lists[listId].kanjis = state.lists[listId].kanjis.filter(
        (item) => item !== kanjiId
      );
      state.lists[listId].page = 1;
      return { ...state };
    }
    case CHANGE_PAGE_KANJI_LIST: {
      return {
        ...state,
        page: action.payload.page,
      };
    }
    case CHANGE_PAGE_IN_KANJI_LIST: {
      const { listId, page } = action.payload;
      state.lists[listId] = { ...state.lists[listId], page };
      return { ...state };
    }
    default:
      return state;
  }
};
