import _ from 'lodash';
import {
  FETCH_WORD,
  GET_WORD_LIST,
  SEARCH,
  FETCH_WORDS,
} from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case SEARCH:
      let wordIds = new Set();
      let words = [];
      for (let result of action.payload.results) {
        for (let word of result.words) {
          if (!wordIds.has(word.id)) {
            words.push(word);
            wordIds.add(word.id);
          }
        }
      }
      return { ...state, ..._.mapKeys(words, 'id') };
    case GET_WORD_LIST:
      return { ...state, ..._.mapKeys(action.payload.words, 'id') };
    case FETCH_WORD:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case FETCH_WORDS:
      return { ...state, ..._.mapKeys(action.payload.words, 'id') };
    default:
      return state;
  }
};
