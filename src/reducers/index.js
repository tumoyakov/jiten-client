import { combineReducers } from 'redux';
import kanjiReducer from './kanjiReducer';
import wordReducer from './wordReducer';
import kanjiPartsReducer from './kanjiPartsReducer';
import authReducer from './authReducer';
import { reducer as formReducer } from 'redux-form';
import searchReducer from './searchReducer';
import kanjiListReducer from './kanjiListReducer';
import wordListReducer from './wordListReducer';
import testReducer from './testReducer';
import radicalReducer from './radicalReducer';
import readerReducer from './readerReducer';
import statisticReducer from './statisticReducer';

export default combineReducers({
  kanjis: kanjiReducer,
  words: wordReducer,
  kanjiParts: kanjiPartsReducer,
  session: authReducer,
  kanjiLists: kanjiListReducer,
  wordLists: wordListReducer,
  search: searchReducer,
  test: testReducer,
  form: formReducer,
  radKeys: radicalReducer,
  reader: readerReducer,
  statistic: statisticReducer,
});
