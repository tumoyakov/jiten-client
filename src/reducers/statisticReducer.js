import _ from 'lodash';
import { GET_STAT, FLUSH_STAT } from '../actions/types';

const initialState = {
  statistic: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_STAT:
      return {
        ...state,
        statistic: action.payload,
      };
    case FLUSH_STAT:
      return { ...state, statistic: null };
    default:
      return state;
  }
};
