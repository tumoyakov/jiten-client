import _ from 'lodash';
import {
  GET_RADICALS,
  FIND_KANJI_BY_RADICALS,
  CLEAR_RESULT_SEARCH_BY_RADS,
} from '../actions/types';

const initialState = {
  radicals: [],
  kanjis: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_RADICALS:
      return { ...state, radicals: action.payload };
    case FIND_KANJI_BY_RADICALS:
      return { ...state, kanjis: action.payload };
    case CLEAR_RESULT_SEARCH_BY_RADS:
      return { ...state, kanjis: [] };
    default:
      return state;
  }
};
