import _ from 'lodash';
import {
  ADD_TEXT,
  FLUSH_READER,
  GET_WORDS_AND_KANJI_FROM_TEXT,
} from '../actions/types';

const initialState = {
  text: null,
  data: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TEXT:
      return { ...state, text: action.payload.text };
    case GET_WORDS_AND_KANJI_FROM_TEXT:
      return {
        ...state,
        data: action.payload,
      };
    case FLUSH_READER:
      return { ...state, text: null };
    default:
      return state;
  }
};
