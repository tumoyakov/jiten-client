import _ from 'lodash';
import { SEARCH, CHANGE_PAGE } from '../actions/types';

const initialState = {
  searchString: '',
  results: [],
  page: 1,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH: {
      const { searchString, results } = action.payload;

      // const kanjisCount =
      //   kanjis && kanjis.length <= 10
      //     ? 0
      //     : kanjis.length % 10 !== 0
      //     ? parseInt(kanjis.length / 10) + 1
      //     : parseInt(kanjis.length / 10);
      // const wordsCount =
      //   words && words.length <= 10
      //     ? 0
      //     : words.length % 10 !== 0
      //     ? parseInt(words.length / 10) + 1
      //     : parseInt(words.length / 10);

      return {
        ...state,
        searchString,
        results,
      };
    }

    case CHANGE_PAGE: {
      return {
        ...state,
        page: action.payload.page,
      };
    }
    default:
      return state;
  }
};
