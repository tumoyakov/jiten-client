import _ from 'lodash';
import {
  SIGN_IN,
  SIGN_OUT,
  SIGN_UP,
  REFRESH_USER,
  CHANGE_PASSWORD,
} from '../actions/types';

const initialState = {
  auth: false,
  accessToken: null,
  user: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN:
    case SIGN_UP: {
      localStorage.setItem('token', action.payload.accessToken);
      return {
        ...state,
        ...action.payload,
      };
    }
    case REFRESH_USER: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case CHANGE_PASSWORD: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case SIGN_OUT: {
      localStorage.removeItem('token');
      return {
        ...state,
        ...initialState,
      };
    }
    default:
      return state;
  }
};
