# Jiten веб-приложение

## Для установки проекта нужно в консоли, находясь в корневой директории проекта выполнить команду

### `npm install`

## Для запуска проекта, нужно в консоли, находясь в корневой директории проекта выполнить команду

### `npm start`

Это запустит приложение в режиме разработки.
Откройте [http://localhost:3000](http://localhost:3000) для просмотра в браузере.
